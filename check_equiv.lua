--[[ Check if my proposed code:
LDA $03
CMP $62
BCC return_false ; C < 3
BEQ keep_checking ; C == 3
BCS return true ; C == 4
keep_checking:
LDA $64
BEQ return false ; No pairs
CLC
ADC $65
CMP $02
BNE return false
return_true:
CLC
RTS
return_false:
SEC
RTS

Is equivalent to Tobita-san's code
]]

function tobita(C,P,S) 
    local num_grps = C+P+S
    if (num_grps >= 5) then
        S = 4-C
        P = (P>=1) and 1 or 0
    end
    
    local sum = 2*C + P + S
    
    return (sum == 8)
end

function me(C,P,S)
    if (C < 3) then return false end
    if (C == 4) then return true end
    if (P > 0 and P+S==2) then return true end
    return false
end

for c = 0,4 do
    for p = 0,6 do
        for s = 0,6 do
            -- Only run check on a possible hand
            if (c*3 + (p+s)*2 <= 13) then
                tobita_sez = tobita(c,p,s)
                me_sez = me(c,p,s)
                print("cpstm", c, p, s, tobita_sez, me_sez)
            end
        end
    end
end
