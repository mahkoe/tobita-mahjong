$FD09#NMI Handler#Save all the registers on the stack
$FE86#Read controller#
$C000#Reset#
$C042##Clear stack
$FE9C##Check for Select
$FEA6##A or B pressed
$FEB1##Start or select NOT pressed
$C06A#do_new_game?#Clear stack
$FD23#skip_sprite_update#Save 2 in $0D (why?)
$FD4A#done_tile_updates#
$FE6F#TIMEWASTER#Wait for vlbank, then some delay, then write $6 to PPU_CTRL
$FE74##Load X = 132, Y = 8, and count down until x == 0 and  y == 0
$FE72##Wait until VSYNC?
$FE78#timewaste_loop#
$FEC5#WHATAMI_00#Has to do with playing music, I guess
$FEC7##If $17 is zero, quit this function right away
$FE91#rd_contrlr_looptop#
$FEA0##If select pressed, save 0xFF in $1C
$FD6D##Restore registers from stack
$FD38##Skip past mode byte
$FD9D#rmw_attributes#Reads the following entry type from the $700 array:
\- VRAM addr hi
\- VRAM addr lo
\- AND mask
\- OR mask
$FE56#GO_TO_NEXT_ROW#This is used in the function for copying a 4x4 tile from the global
\LUT. This... futzes around with the VRAM address to go to the
\right place
\
\Re-read tile update read pointer (because the calling code has
\been incrementing its current index into the tile update array)
$CA34#wait_for_ppu_to_finish#
$CC64##Up pressed?
$CC70##Down pressed?
$CA0C#SOME_FN_01#
$CA13##Left pressed?
$CA24##Right pressed?
$C53E#SOME_FN_02#
$FD1C##Set $04 = 0
$FD0E##Check if $04 is nonzero
$FAB9##Set 4 to 0xFF
$C9B8#dont_run_some_fn_01#
$C040#SOME_JMP_TARGET_00#Jumped from:
\- C00F
$F7D5#RNG#Seems to take A as an argument, although it's using a bunch of
\global registers as seed values. Some of which it edits probably
\in order to maintain state.
\
\Note: there must be some way that its output is constrained to a
\certain range using some zero page values, but I haven't
\bothered to understand the details yet.
\
\Called from:
\- C045
\- CF7A
\
\Save A in $1D and then push X to the stack
$C043#wait_for_ppu_powerup#
$F7CA##Overwrite hand[cur] with value from hand[cur+1]
$F67B#DELETE_TILE_FROM_STRING#Deletes first instance of tile ID in A from ahnd struct pointed to
\by X and Y. Does not seem to update the fields of the hand 
\struct dealing with the count of concealed tiles.
\
\Called from:
\- CA7E
\- C6FA
\
\Save X and Y into the hand_ptr
$F796#SORT_HAND#Place {X,Y} (address of one player's hand) into {$10,$0F}, then
\sort hand in place using ordinary bubble sort
$F79C##Set $9A to $FF
$F79E##SET Y to 0
$F79F##Get first tile of selected player's hand
$F7B2##Set X to 0. In this loop, X is...
$F7B3##Set $9A to 0. If we performed a swap, we will set it to $FF;
\this indicates that another bubble sort pass is needed
$F7B5#sort_adjacent_pairs_looptop#$97++. Before the first iteration, we have loaded $FF to $97, 
\so this will be zero. In this loop, $97 stores cur_idx into the
\selected hand
$F7B7##Y = $97+1, the tile to the right of our current tile
$F7C1##A contains tile at Y=cur_idx+1. This tests if the next tile is 
\strictly less than this one (I think this can only happen with
\the drawn tile)
$F7C5##Save tile at idx+1 to X
$F7C6##Get tile at cur_idx in A
$F7C8##Save hand[cur] on the stack so we can put hand[cur+1] back in A
$F7CD##Restore tile from stack (the one we just overwrote by shifting
\down hand[cur+1] to hand[cur]
$F7CE##Put it at hand[cur+1]. Essentially, this completes a swap
$F7AA#bubble_sort#In this loop, $9A is $FF if we performed a swap. If we didn't swap
\anything, then it means the array was sorted
$FBDD##An earlier check made sure that there was more than one tile, and
\this is after sorting the AI's hand
$C606##Sort top player's hand
$FB44##For some reason, re-save XY into HAND_BASE pointer
$FB4A##Get first tile of hand
$FB4C##Let Y point to next tile in hand
$FB4D##Is first tile $FF? If so, idk what that means
$FBD9#REGISTER_HAND_DISPLAY_UPDATES?#No clue what $0C is for... maybe in this case just to backup the
\A register so it doesn't get clobbered?
$FD17##Write the 256 bytes at $600 to sprite memory
$FD89#write_tile_update_str#
$FD8A##Read a byte. Quit loop if it is 0, otherwise we'll use it
$FD7C#addr_zeroterm_tile_update#The format of the array at $700 is a sort of circular buffer of
\variable-length entries that start with an address and a zero-
\terminated array of tile numbers
$FCF7#APPEND_TO_TILE_UPDATES#Looks like this might write the current A at tile_updates[++$1]
\so long as it not equal to the value in $0 (the address of the 
\last tile update). I guess this is to prevent buffer overflows;
\if there isn't enough space in 256 bytes for all the tile updates
\on this frame, we will defer work or something,
\
\Actually, this probably implements a sort of event queue between
\the main thread and the NMI handler in a producer-consumer
\pattern
$FCF8##Save Y in $98 because we are about to use it for something else
$FD02##Not sure why we pushed A only to pop it. Probably this is because
\this is some kind of main loop where the NMI might empty a queue
\and as a result A might change? But the NMI takes care of 
\restoring A already so that can't be right...
$FD06##Restore Y
$FBCA##Put in the zero-terminator
$FD33##Read tile update type:
\0 - Do nothing
\1 - Addr + ANDmask + ORmask for attribute
\2 - Unknown
\3 - Unknown
\4 - Addr + Zero-terminated array of tile numbers
$FD2D#tileup_looptop#I ahve no idea what $2 is being used for... but in the nametable
\update loop we keep returning here
$FDAD##Read existing attribute. We need to read twice because that's
\just how VRAM reads work; apparently the first one is always 
\bogus
$FDA5##Load lo bits of VRAM addr
$FDA4##Save VRAM addr hi in X
$FDB3##AND the old attributes with the next entry in our update array,
\then OR that result with the entry after that
$FDBB##Save updated read pointer
$FDBD##Save bitmasking result in Y
$FDBE##We saved VRAM hi in X earlier so we could repeat the address
\and write our result, which is what we're doing now
$FDC1##Pop VRAM lo from the stack
$FDAC##Push VRAM lo on the stack
$FDC5##Write our masked value back to attribute memory
$FD79##Strange... a deferred jump... this must have been the patch
\that was applied in November 1984
$FDCB#write_tile4x4_from_lut#First reads VRAM addr, then first offset into global LUT at
\$FF24. Will essentially do memcpy(VRAM_Addr,$FF24+offset,4)
$FDD9##Read offset (used later) into X
$FDDD##Save updated read pointer
$FF24#SOME_TILE_LUT[0]#
$FF25#SOME_TILE_LUT[1]#
$FF26#SOME_TILE_LUT[2]#
$FF27#SOME_TILE_LUT[3]#
$FF28#SOME_TILE_LUT[4]#
$FF29#SOME_TILE_LUT[5]#
$FF2A#SOME_TILE_LUT[6]#
$FF2B#SOME_TILE_LUT[7]#
$FF2C#SOME_TILE_LUT[8]#
$FF2D#SOME_TILE_LUT[9]#
$FF2E#SOME_TILE_LUT[A]#
$FF2F#SOME_TILE_LUT[B]#
$FF30#SOME_TILE_LUT[C]#
$FF31#SOME_TILE_LUT[D]#
$FF32#SOME_TILE_LUT[E]#
$FF33#SOME_TILE_LUT[F]#
$FF34#SOME_TILE_LUT[10]#
$FF35#SOME_TILE_LUT[11]#
$FF36#SOME_TILE_LUT[12]#
$FF37#SOME_TILE_LUT[13]#
$FF38#SOME_TILE_LUT[14]#
$FF39#SOME_TILE_LUT[15]#
$FF3A#SOME_TILE_LUT[16]#
$FF3B#SOME_TILE_LUT[17]#
$FF3C#SOME_TILE_LUT[18]#
$FF3D#SOME_TILE_LUT[19]#
$FF3E#SOME_TILE_LUT[1A]#
$FF3F#SOME_TILE_LUT[1B]#
$FF40#SOME_TILE_LUT[1C]#
$FF41#SOME_TILE_LUT[1D]#
$FF42#SOME_TILE_LUT[1E]#
$FF43#SOME_TILE_LUT[1F]#
$FF44#SOME_TILE_LUT[20]#
$FF45#SOME_TILE_LUT[21]#
$FF46#SOME_TILE_LUT[22]#
$FF47#SOME_TILE_LUT[23]#
$FF48#SOME_TILE_LUT[24]#
$FF49#SOME_TILE_LUT[25]#
$FF4A#SOME_TILE_LUT[26]#
$FF4B#SOME_TILE_LUT[27]#
$FF4C#SOME_TILE_LUT[28]#
$FF4D#SOME_TILE_LUT[29]#
$FF4E#SOME_TILE_LUT[2A]#
$FF4F#SOME_TILE_LUT[2B]#
$FF50#SOME_TILE_LUT[2C]#
$FF51#SOME_TILE_LUT[2D]#
$FF52#SOME_TILE_LUT[2E]#
$FF53#SOME_TILE_LUT[2F]#
$FF54#SOME_TILE_LUT[30]#
$FF55#SOME_TILE_LUT[31]#
$FF56#SOME_TILE_LUT[32]#
$FF57#SOME_TILE_LUT[33]#
$FF58#SOME_TILE_LUT[34]#
$FF59#SOME_TILE_LUT[35]#
$FF5A#SOME_TILE_LUT[36]#
$FF5B#SOME_TILE_LUT[37]#
$FF5C#SOME_TILE_LUT[38]#
$FF5D#SOME_TILE_LUT[39]#
$FF5E#SOME_TILE_LUT[3A]#
$FF5F#SOME_TILE_LUT[3B]#
$FF60#SOME_TILE_LUT[3C]#
$FF61#SOME_TILE_LUT[3D]#
$FF62#SOME_TILE_LUT[3E]#
$FF63#SOME_TILE_LUT[3F]#
$FF64#SOME_TILE_LUT[40]#
$FF65#SOME_TILE_LUT[41]#
$FF66#SOME_TILE_LUT[42]#
$FF67#SOME_TILE_LUT[43]#
$FF68#SOME_TILE_LUT[44]#
$FF69#SOME_TILE_LUT[45]#
$FF6A#SOME_TILE_LUT[46]#
$FF6B#SOME_TILE_LUT[47]#
$FF6C#SOME_TILE_LUT[48]#
$FF6D#SOME_TILE_LUT[49]#
$FF6E#SOME_TILE_LUT[4A]#
$FF6F#SOME_TILE_LUT[4B]#
$FF70#SOME_TILE_LUT[4C]#
$FF71#SOME_TILE_LUT[4D]#
$FF72#SOME_TILE_LUT[4E]#
$FF73#SOME_TILE_LUT[4F]#
$FF74#SOME_TILE_LUT[50]#
$FF75#SOME_TILE_LUT[51]#
$FF76#SOME_TILE_LUT[52]#
$FF77#SOME_TILE_LUT[53]#
$FF78#SOME_TILE_LUT[54]#
$FF79#SOME_TILE_LUT[55]#
$FF7A#SOME_TILE_LUT[56]#
$FF7B#SOME_TILE_LUT[57]#
$FF7C#SOME_TILE_LUT[58]#
$FF7D#SOME_TILE_LUT[59]#
$FF7E#SOME_TILE_LUT[5A]#
$FF7F#SOME_TILE_LUT[5B]#
$FF80#SOME_TILE_LUT[5C]#
$FF81#SOME_TILE_LUT[5D]#
$FF82#SOME_TILE_LUT[5E]#
$FF83#SOME_TILE_LUT[5F]#
$FF84#SOME_TILE_LUT[60]#
$FF85#SOME_TILE_LUT[61]#
$FF86#SOME_TILE_LUT[62]#
$FF87#SOME_TILE_LUT[63]#
$FF88#SOME_TILE_LUT[64]#
$FF89#SOME_TILE_LUT[65]#
$FF8A#SOME_TILE_LUT[66]#
$FF8B#SOME_TILE_LUT[67]#
$FF8C#SOME_TILE_LUT[68]#
$FF8D#SOME_TILE_LUT[69]#
$FF8E#SOME_TILE_LUT[6A]#
$FF8F#SOME_TILE_LUT[6B]#
$FF90#SOME_TILE_LUT[6C]#
$FF91#SOME_TILE_LUT[6D]#
$FF92#SOME_TILE_LUT[6E]#
$FF93#SOME_TILE_LUT[6F]#
$FF94#SOME_TILE_LUT[70]#
$FF95#SOME_TILE_LUT[71]#
$FF96#SOME_TILE_LUT[72]#
$FF97#SOME_TILE_LUT[73]#
$FF98#SOME_TILE_LUT[74]#
$FF99#SOME_TILE_LUT[75]#
$FF9A#SOME_TILE_LUT[76]#
$FF9B#SOME_TILE_LUT[77]#
$FF9C#SOME_TILE_LUT[78]#
$FF9D#SOME_TILE_LUT[79]#
$FF9E#SOME_TILE_LUT[7A]#
$FF9F#SOME_TILE_LUT[7B]#
$FFA0#SOME_TILE_LUT[7C]#
$FFA1#SOME_TILE_LUT[7D]#
$FFA2#SOME_TILE_LUT[7E]#
$FFA3#SOME_TILE_LUT[7F]#
$FFA4#SOME_TILE_LUT[80]#
$FFA5#SOME_TILE_LUT[81]#
$FFA6#SOME_TILE_LUT[82]#
$FFA7#SOME_TILE_LUT[83]#
$FFA8#SOME_TILE_LUT[84]#
$FFA9#SOME_TILE_LUT[85]#
$FFAA#SOME_TILE_LUT[86]#
$FFAB#SOME_TILE_LUT[87]#
$FFAC#SOME_TILE_LUT[88]#EA is a blank tile
$FFAD#SOME_TILE_LUT[89]#
$FFAE#SOME_TILE_LUT[8A]#
$FFAF#SOME_TILE_LUT[8B]#
$FFB0#SOME_TILE_LUT[8C]#
$FFB1#SOME_TILE_LUT[8D]#
$FFB2#SOME_TILE_LUT[8E]#
$FFB3#SOME_TILE_LUT[8F]#
$FFB4#SOME_TILE_LUT[90]#
$FFB5#SOME_TILE_LUT[91]#
$FFB6#SOME_TILE_LUT[92]#
$FFB7#SOME_TILE_LUT[93]#
$FFB8#SOME_TILE_LUT[94]#
$FFB9#SOME_TILE_LUT[95]#
$FFBA#SOME_TILE_LUT[96]#
$FFBB#SOME_TILE_LUT[97]#
$FFBC#SOME_TILE_LUT[98]#
$FFBD#SOME_TILE_LUT[99]#
$FFBE#SOME_TILE_LUT[9A]#
$FFBF#SOME_TILE_LUT[9B]#
$FFC0#SOME_TILE_LUT[9C]#
$FFC1#SOME_TILE_LUT[9D]#
$FFC2#SOME_TILE_LUT[9E]#
$FFC3#SOME_TILE_LUT[9F]#
$FFC4#SOME_TILE_LUT[A0]#
$FFC5#SOME_TILE_LUT[A1]#
$FFC6#SOME_TILE_LUT[A2]#
$FFC7#SOME_TILE_LUT[A3]#
$FFC8#SOME_TILE_LUT[A4]#
$FFC9#SOME_TILE_LUT[A5]#
$FFCA#SOME_TILE_LUT[A6]#
$FFCB#SOME_TILE_LUT[A7]#
$FFCC#SOME_TILE_LUT[A8]#
$FFCD#SOME_TILE_LUT[A9]#
$FFCE#SOME_TILE_LUT[AA]#
$FFCF#SOME_TILE_LUT[AB]#
$FFD0#SOME_TILE_LUT[AC]#
$FFD1#SOME_TILE_LUT[AD]#
$FFD2#SOME_TILE_LUT[AE]#
$FFD3#SOME_TILE_LUT[AF]#
$FFD4#SOME_TILE_LUT[B0]#
$FFD5#SOME_TILE_LUT[B1]#
$FFD6#SOME_TILE_LUT[B2]#
$FFD7#SOME_TILE_LUT[B3]#
$FFD8#SOME_TILE_LUT[B4]#
$FFD9#SOME_TILE_LUT[B5]#
$FFDA#SOME_TILE_LUT[B6]#
$FFDB#SOME_TILE_LUT[B7]#
$FFDC#SOME_TILE_LUT[B8]#
$FFDD#SOME_TILE_LUT[B9]#
$FFDE#SOME_TILE_LUT[BA]#
$FFDF#SOME_TILE_LUT[BB]#
$FFE0#SOME_TILE_LUT[BC]#
$FFE1#SOME_TILE_LUT[BD]#
$FFE2#SOME_TILE_LUT[BE]#
$FFE3#SOME_TILE_LUT[BF]#
$FFE4#SOME_TILE_LUT[C0]#
$FFE5#SOME_TILE_LUT[C1]#
$FFE6#SOME_TILE_LUT[C2]#
$FFE7#SOME_TILE_LUT[C3]#
$FFE8#SOME_TILE_LUT[C4]#
$FFE9#SOME_TILE_LUT[C5]#
$FFEA#SOME_TILE_LUT[C6]#
$FFEB#SOME_TILE_LUT[C7]#
$FFEC#SOME_TILE_LUT[C8]#
$FFED#SOME_TILE_LUT[C9]#
$FFEE#SOME_TILE_LUT[CA]#
$FFEF#SOME_TILE_LUT[CB]#
$FFF0#SOME_TILE_LUT[CC]#
$FFF1#SOME_TILE_LUT[CD]#
$FFF2#SOME_TILE_LUT[CE]#
$FFF3#SOME_TILE_LUT[CF]#
$FFF4#SOME_TILE_LUT[D0]#
$FFF5#SOME_TILE_LUT[D1]#
$FFF6#SOME_TILE_LUT[D2]#
$FFF7#SOME_TILE_LUT[D3]#
$FFF8#SOME_TILE_LUT[D4]#
$FFF9#SOME_TILE_LUT[D5]#
$FFFA#SOME_TILE_LUT[D6]#
$FFFB#SOME_TILE_LUT[D7]#
$FFFC#SOME_TILE_LUT[D8]#
$FFFD#SOME_TILE_LUT[D9]#
$FFFE#SOME_TILE_LUT[DA]#
$FFFF#SOME_TILE_LUT[DB]#
$FDDF##Read tile ID from that global LUT. We will actually read several
\tile IDs in a row and write them all. Note that the OAMADDR
\register autoincrements after every write to OAMDATA
\(aka PPUADDR and PPUDATA)
$FE5A##We will need to add $20 to VRAM_addr_lo to get to the next
\row
$FE58##Decrement Y to "undo" updates done by outer code. This ever-
\so-slightly breaks the very clean code from Tobita-san, so if I had
\to guess, this was part of the November 1984 patch
$FE60##Push our modified VRAM addr lo
$FE61##Decrement Y one more time so that it indexes the VRAM addr hi
$FE62##We're adding zero here so that we can include the carry bit
$FE67##Set our updated VRAM address
$F2A5#waiting_loop?#
$F2BE#busy_loop#Don't know what this is for... probably to prevent game from
\running too fast
$F2C6#done_busy_loop#
$C4EF#SOME_FN_03#Called from:
\- C0B6
$E200#SOME_FN_04#Called from:
\- C0BB
$DB4F#SOME_FN_05#Called from:
\- C0BE
\
\Zero out a bunch of stuff
$DB59##Load A from $39... whatever that is
$DB60##I guess this does $6E = ($39 + 1) % 4
$F465#LOAD_HAND_PTR#A is the hand number. A and $98 are clobbered
\Called from
\- DB6E
\
\Save A in $98
$F467##Push X to the stack
$F469##Restore A
$F47B#HAND_PTR_LUT[0]#
$F46B##Multiply A by two for pointer arithmetic
$F47C#HAND_PTR_LUT[1]#
$F47D#HAND_PTR_LUT[2]#
$F47E#HAND_PTR_LUT[3]#
$F47F#HAND_PTR_LUT[4]#
$F480#HAND_PTR_LUT[5]#
$F481#HAND_PTR_LUT[6]#
$F482#HAND_PTR_LUT[7]#
$DB71##Copy HAND_PTR_LO to $40 and $A2? Why?
$DB77##Copy HAND_PTR_HI to $41 and $A3? Why?
$F70A#COPY_HAND_TO_200#It seems Tobita-san's calling convention is:
\- A is argument, and may be clobbered
\- X and Y are always saved
\
\Called from:
\- DB7D
\- D0C7
\
\Push X and Y to the stack
$F712#hand_cpy#Seems to copy 70 bytes from current hand ptr to $200
\
\Get next tile from hand
$F714##Copy to $200
$F71B##Restore X and Y
$F6AF#APPEND_A_TO_TILELIST#A is tile to append. Its value is saved.
\X and Y are arguments hand_ptr HI and LO. Their values are
\saved
\
\Called from:
\
\- DB86
$DB82##Put address of temp hand ($200) into HAND_PTR
$F6B6#append_to_tiles_looptop#
$F6B3##Save A to stack
$F6BD##Copy stack top to A (but don't pop)
$F6C4##Replace sentinel
$F6C6##Restore X and Y
$DB89##This is zeroing out some portion of the player's hand. Recall
\that {A2,A3} contains the hand pointer (not sure why we
\can't just reuse {0F,10})
$DB8B##This must be the offset of some member of the hand struct.
\Here we are zeroing it out
$DB99##Write a sentinel after those zeros
$DE0F#SOME_FN_06#Called from
\- DB9D
\
\Save 1 in $61 for some reason
$E41D#CHECK_WINNING_HAND?#Called from:
\- DE13
$E435#CHECK_HAND_COMPLETE#This checks if the tmp_hand has a pair and four groups (or if it
\has chiitoitsu). idk how it's supposed to deal with called sets. If
\the hand is incomplete, sets carry before returning. If the hand is
\complete, not only does it return with clear carry, but it also sets
\$26 to a truthy value
\
\Called from:
\- E41D
\
\Save 0 into $30, $26, and $2E
$ED5F#IS_13_ORPHANS?#Checks tmp_hand. If a simple is found, sets carry and returns.
\Otherwise, does some kind of further check, probably related to
\13 orphans
\
\Called from:
\- E43D
\
\I'll bet this is clearing those entries so it can check for 13 orphans
$E5D8#CLEAR_03F6_THRU_041E_TO_FF#Also clears $23, $27, $28, $2D, and $2F. Guess: these are flags
\for individual yaku checks
\
\Called from:
\- ED5F
\- EEA3
\
\Save 0 into $23, $27, $28, $2D, and $2F
$E5EA#clear_stuff_looptop#
$ED62##Read tile from tmp_hand at $200
$ECA6#TILE_IS_HONOUR_OR_TERMINAL#A contains tile ID. If a simple tile, function sets carry and returns
\
\Called from:
\- EDC6
\
\Check if this is an honour tile
$EC94#is_suit_tile#Check again if it's an honour tile?
$ECA2#return_carry_clear#
$ECA4#return_carry_set#
$EC98##Get number within suit
$ECA8##If tile ID is >= to $30 (meaning it's an honour tile)
$EC9A##Return carry clear if this is a terminal
$ED6F##Branch if honour/terminal
$ED64#is_all_honours_or_terminals_looptop#
$ED72#hand_had_no_simples#
$E440##Quit fn if all honours/terminals
$EE90#CHECK_YAKU?#Seems to expect an input in $61 and in $3D. If $61 is nonzero or
\if $3D gte 4 this returns with carry set. If the hand has any open
\sets this returns with carry set
\
\Called from:
\- E442
$EE94##Branch if $3D was gte 4
$EEFD#return_carry_set_check_yaku#
$F720#FIND_IDENTICAL_TILE_GROUPS#Saves all tiles with more than one copy in the hand to a FF-
\terminated list starting at $446, and saves count of these
\groups in $64
\
\Called from:
\- E447
\
\Zero out $64 and $9D and registers
$F652#FIND_SMALLEST_ABOVE_A#Seems like maybe it finds the min of the array at $200, but there
\is some extra condition that it must be greater than whatever
\was in A. A will contain min, nothing else is clobbered
\
\Sets carry if A was $FF?
\
\Called from:
\- F72C
\
\Save A to $97
$F654##Push X
$F658##Save X to $98
$F65B#find_smallest_above_looptop#
$F669##If tile[X] was <= $97
$F665##If tile[X] was gt $98
$F66D##Save A over $98
$F671#find_smallest_above_doneloop#
$F673##Save min in A
$F677##Just return if A was $FF
$F72C##I think this function can only set carry if A was $FF
$F731##Save search tile in $9D
$F539#COUNT_IDENTICAL_TILES#A is tile to search for. X is first offset into tmp_hand.  Count is
\returned in A
\
\Called from:
\- F735
\- CE4D
\
\Save A in $98
$F53B##Push X and Y 
$F733##Setting X to zero here turns out to be important? In the next
\function call X is this initial offset of where we're searching
$F541#count_identical_tiles_looptop#
$F53F##Y will contain our count of identical tiles
$F550#count_identical_tiles_loopdone#Save count into $98
$F552##Restore X and Y
$F738##Check if count of identical tiles was 1
$F72A#find_identical_tile_group_looptop#
$F726##9D starts out as zero, but then gets set to a tile ID later
$F746#find_identical_tile_group_loopdone#Terminate list of groups with sentinel
$F744##I think this branch will be always taken (unless there are 256
\tile groups in the hand o_0)
$E447##The list of groups will be sentinel-terminated at $446. The count
\of groups will be in $64, though I don't think that count gets
\used
$E44A##At this point, we will iterate through all the tile IDs that appear
\more than once and brute-force try all of them as the head of
\the hand (i.e. the pair).
\
\Start X at 0
$E44F##Save A in $98. It seems that Tonita-san always uses $98 to
\save A when A would otherwise be clobbered (and I don't see
\him using $98 for anything else)
$E451##Check for the sentinel, telling us there are no more pairs to try
$E48C#done_trying_pair_candidates#Load number of pair candidates that FIND_IDENTICAL_TILE_GROUPS
\left in $64
$E455##Increment X then push it
$E458##Restore A
$E45A##Copy A to $2C (who knows why)
$E45E##Set X to zero, probably in anticipation of calling the next function
$F6F7#FIND_TILE#Assmes that the needle tile ID will be saved in $9C. Also seems
\to want X to be an offset (probably 0) into the tmp_hand array.
\Unlike the other functions that return in A, this one returns the
\index of the search tile in Y. It returns with carry set of the tile
\was not found
\
\Called from:
\ - E460
\ - E243
\
\Copy X to A and Y.
$F6F9##Decrement Y... then reincrement it? Maybe this was a bug fix?
\Or maybe Tonita-san wanted to save space for future patches?
\Neither of those two guesses sound right... maybe it has to do
\with setting the flags?
$F6FA#some_fn_12_looptop#
$F708#return_carry_set_some_fn_12#
$E463##Push tmp_hand[X] to stack
$E467##Take found tile and write it to index X
$E434#return_carry_clear_check_hand_complete#
$F728##Zeroes out X and Y
$E45C##Actually, saving it in $9C is necessary for the FIND_TILE function
$E46D##Restore value from tmp_hand[X] into A, then write it to 
\tmp_hand[Y]. This completes a swap between tmp_hand[X] and
\tmp_hand[Y]
$E471##Increment X so next FIND_TILE call will skip past where we just
\moved the last one
$E475##Swap tmp_hand[X] and tmp_hand[Y]
$E49C#PULLDOWN_GROUPS#Assumes X contains current index into tmp_hand. Will find the
\mintile ID starting from X then move it to position X (by swapping
\out what used to be there) then attempt to pull down a group of 
\three or four identical tiles into positions X+1 and X+2 (and X+3). 
\If that fails, it tries a sequence. If that fails, the the carry is 
\set and the function returns. Else, the function recurses on the 
\rest of the hand.
\
\By the way, I'm pretty sure this is only called after a special case
\that already puts a pair (the first pair it could find) into indices
\0 and 1 of tmp_hand.
\
\Called from:
\
\ - E484
\
\Push X to stack
$E49E##Load tmp_hand[X]
$E4A1##Check for sentinel... not sure why this would happen
$F637#FIND_MIN_TILE#Assumes X is first index into tmp_hand. X will not be clobbered.
\Min tile is returned in A.
\
\Called from:
\ - E4A5
\
\Push X to stack
$F639##Save $FF into $98
$F63D#find_min_tile_looptop#Read the next tile ID
$F647##Branch if A was gte to $98
$F649##If A was strictly less than $98, save it in $98.
$F64D#find_min_tile_loopdone#Restore X
$F64F##Put min tile ID into A
$F4E5#PULLDOWN_SINGLE_PON_OR_KAN#Takes tile ID in A, and first index in X. Searches the tmp_hand
\(starting at X) for all tiles equal to A, and returns with carry clear
\if there are 3 or more. If a pon/kan is found, it will be swapped
\down to indices X,X+1,X+2 and X is increased
\
\A is clobbered.
\
\Called from:
\
\- E4A8
\
\Save A to $98
$F4EA##Restore A
$F4EC##Write A to 9C, because COUNT_IDENTICAL_TILES takes its
\argument from there
$F4E7##Save $9C to stack so we don't clobber it
$F4FA#was_a_triple_or_quad#I think this percolates the triple/quad down
$F4F5##Restore original value of $9C
$E4B3#not_pon#
$F483#PULLDOWN_SINGLE_SEQUENCE#Assumes X is current position in tmp_hand. Will search for a
\sequence of 3 tiles starting with the tile id in A and move them
\all to tmp_hand[X], tmp_hand[X+1], tmp_hand[X+2]. Otherwise,
\will set carry and return if no sequence found
\
\Does NOT increment X
\
\This can edit the tmp_hand even if no sequence is found
\
\Called from:
\- E4B6
\
\Check if A is an honour tile
$F4E3#return_carry_set_find_sequence#
$F487##Save A to $98
$F489##Push $9C, X, and Y to the stack so we can restore them later
$F490##Restore A
$F492##Put A into $9C because that is how FIND_TILE takes its
\argument
$F4DC#restore_return_carry_set_find_sequence#
$F499##Swap found tile with position X
$F4A8##Look for tile that comes right after this one
$F4AD##Return early if not found
$F4AF##Swap found tile with position X
$F4A7##Move to next position
$F4BD##Now look for the third tile in the sequence
$F4C5##Swap found tile with position X
$F4D3##Move to next position
$F4D4##Restore $C9 and X and Y
$F4DA##Make sure to clear carry so outside code knows we found a 
\sequence
$E4C1#no_sequence_found#
$E472##Now find the second tile from this prospective pair
$E483##Move to next index and pulldown groups
$E44C#try_next_pair_candidate#
$E487##Restore X, which was the index of the next pair candidate
$E48E##Check for chiitoitsu!
$E495#not_chiitoitsu#
$E4C5#hand_is_complete#Save the fact that we found a complete hand
$EDAC#CHIITOITSU#Calling it now: this function checks there are no kans
$E49B#no_complete_hand_found#
$E625#SOME_FN_18#Called from:
\ - E5D4
\
\Set X = 1, A = 0, and $9D = 0
$E5A3#SOME_FN_19#This probably checks yakus. This function can write a truthy
\value to $2E
\
\Called from:
\ - E5A3
$EF48#SOME_FN_20#Called from:
\ - 
\
\idk what this is doing
$F97A#INIT#
$FA53##This waits for the first NMI to update the frame counter
$FA4D#WAIT_FOR_NMI#Seems like it also sets up some globals
$F99F##Load address of that tile LUT in FFB4 into X and Y in order to
\pass it as an argument to the next function call
$FCBC#REGISTER_VRAM_WRITE#Looks like it takes {hi,lo} parts of a tile array address in {X,Y}. 
\It also takes a base VRAM address in {$D,$E}. It then returns
\an updated address in {X,Y}
\
\Called from:
\- F9A3, to load image palette
\- Fallen through from FCB9, I don't know for what use yet
\
\Note: here we use the current hadn pointer as a general tile
\array pointer
$FCC2##Save A (the initial write pointer) on stack
$FCD1##This loop ends when we read zero from the LUT. Here, we save
\the CPU flags from reading the value so we don't have to do a
\dummy read later
$FCCF#register_vram_write_looptop#This loop is reading from the global array to construct the initial
\updates array. Specifically, here we copy in the data that will
\go into VRAM
$FCD5##$0A seems to be counting something... but idk what
$FCDF##We held on to the number of things we read from our global LUT
\in Y, and now we're adding it to the hand_ptr, being careful to
\carry into the hi byte if needed. We 'll save the updated lo byte
\in Y, and the updated hi byte in A
$FCE6##Save A to $98
$FCE8##Zero-terminate our display update
$FCED##Restore A then save it in X. This value is the initial wrptr that we
\pushed near the top of thes subroutine
$FCEF##This is the tile update mode. Number 4 is the simple one where
\you just give an address and a zero-terminated string to copy.
\Notice that we set the mode at the very end; because we left the
\original value at the origina wrptr at 0, it was safe for the NMI to
\happen at any time. This is because the NMI ignores any tile
\updates with mode 0.
$FCF4##Save our incremented address hi byte into X (we saved into $98
\earlier to avoid clobbering it)
$FCC3##When I single-stepped this in the debugger, {$D,$E} contained
\the address of the image palette in the PPU VRAM.
$F997##This loads $3F08 into {$D,$E} for the sake of calling the
\REGISTER_VRAM_WRITE function.
$FA2E#WRITE_SPRITE_DESCRIPTOR_LINE_COMMON#Deals with four adjacent tile descriptors or something.
\
\Called from:
\ - F9AF
\ - F9B2
$FA3F#WRITE_SPRITE_DESCRIPTOR_COMMON#Seems to take an argument in A and Y. Also seems to assume that 
\an offset has been set up in X. It writes the 16-bit value {Y,A}
\to $600+X and $600+X+1. X will be incremented by four before
\the function returns. I think this is because this function does the
\parts that are common to all the initial sprite entries, and the
\specific ones are probably done elsewhere
\
\The values of A and Y are presevered
\
\Called from:
\ - FA30
\ - FA35
\ - FA38
$FA3D##Clever fall-through trick here. Instead of JSRing to the
\write_descriptor function, having that RTS, then RTSing here,
\just "jump" to the write_descriptor function directly (no need to 
\actually jump because the code is right next to us). That one
\already has an RTS so it will have to effect of returning from
\_this_ function.
$FA23#WRITE_SPRITE_DESCRIPTOR_LINE_COMMON_CONSTANTS#Starts just two instructions before WRITE_SPRITE_
\DESCRIPTOR_LINE_COMMON, and those two instructions just
\put some constants into Y and A
$F9AF##The next few dozen lines are setting up the initial sprite table
$F9D7#INIT_DISPLAY#Seems to set up a few things on the PPU and to clear out the
\name table to all empty tiles. Seems to use $05 as an argument.
\I think this is how you tell it what nametable to clear.
\
\Called from:
\ - F9D0
\
\Start by waiting for the NMI to read the entire display updates
\array.
$F9DD##For some reason, wait for another frame to go by
$F9E0##Do something about setting up the PPU control register
$F9F1##EA is the ID for an empty tile
$F9E7##Set PPU VRAM address to ($5 << 8). When I single-stepped this
\$5 had the value $24, but I don't know how that got there.
\Anyway, $2400 in VRAM is the address of nametable 1
$F9F7#clear_nametable#
$FA04#keep_clearing_nametable0#
$FA0A##Write $1E to $07. Of course, I still haven't figure out what this
\means...
$FA19#call_some_fn_22_sixteen_times#
$FA7C#SOME_FN_22#no idea what this is rn
$F9CC##It seems $05 is used by INIT_DISPLAY to select the nametable
\and attribute table to clea
$F9D3##Now select nametable and attribute table 0
$CF42#INIT_WALL#This function seems to be initializing memory locations
$F44A#STEP_TILE#This function takes the value in $22 and adds 1, taking care to
\properly jump up when we would select an invalid tile ID, or going
\to 0xFF when we have gone past the last valid ID
$F460#past_last_tile_id#
$F45F#no_adjustment_needed#
$CF46##This function does something to the value at $22. I have no idea
\what it's doing.
$CF4F##Initialize $246-$345 with wall tiles followed by all Fs. Each value
\is copied 4 times
$F44E##IF $22 is now equal to $38, we will set it to all Fs
$CF61#done_generating_wall#This seems to be checking if you have more than one
\controller or something
$C067##Sets up player hands
$CF78#INIT_PLAYERS#Randomly selects first player and sets initial scores. Uses A as
\random seed
\
\Called from:
\ - C067
$F7D9##idk what $1f contains, but it was $C0 when I was single-
\stepping after reset
$F7DB##Rotate left 3 bits, XOR with $20, then rotate right
$F7DE##idk what $20 means, but it was $36 when I was single-stepping
\after reset
$F7E1##Push flag bits. I guess we're interesd in using the carry bit later
$F7E2##Load $1F again (it hasn't changed)
$F7E4##XOR it with $20 (no idea why) then save it back in place
$F7E8##What the heck? We also XOR a whole bunch of other things with
\$20
$F7EC##But this time we save back into $20. Weird
$F7EE##Restore our saved CPU flags
$F7EF##I guess this is grabbing the carry bit from that earlier rotation.
\No clue what any of this for
$F7F5##It's all bit manipulations with global variable for this whole
\function, and I don't know what any of them are for!
$F80D#skip_some_bit_manips#
$CF83##Zero out $3B, $3C, and $37
$CF8F##Set the initial player scores. These are divided by 10 (since all
\Mahjong scores are 0 mod 10, no need to hold on to the extra
\digit)
$D304#RUN_MENU#Returns with carry set when in demo mode, or carry clear in
\player mode
$D30F#skip_init_players#If we got here it's because $12 was nonzero, but now we set it
\to zero
$FAFA#SOME_FN_26#Called from:
\ - D313
$FA17#SOME_FN_27#Called from:
\ - FAFA
$FB0B#long_delay#no idea why this is here
$D3A5##Set up some values in X and Y for use with the next function
$FB14#SOME_FN_28#Called from:
\ - D3A9
$F429#SHUFFLE_WALL#Called from
\ - C072
\
\This is your classic array shuffling algorithm. You put one pointer
\to the last element, and move it down by one each step. Also, 
\at each step, you roll a random number to decide another index
\that you will swap the currently-pointed-element with.
$F434##Put our random number in X. It seems this RNG function has
\bells and whistles to output a value in a specific range
$F42D#shuffle_wall_looptop#
$F445##Whne our top index gets to 0 we can quit
$F437##Swap WALL[X] with WALL[Y]
$F42F##Don't need to do this inside the loop but whatever
$CFBF#CLEAR_HANDS_AND_MISC_WALL_STATE#Called from:
\ - C075
\
\$46 is the number of tiles left after everyone draws their hand.
\I don't think it's a coincidence.
$CFC3##Now we set a BUNCH of variables to 0
$CFE3##This MUST be the dora
$CC3D#DECREMENT_TILES_LEFT#Given tile ID in A, decrement the global count of tiles left. No
\registers are clobbered.
\
\Called from:
\ - CFE6
\
\Save A to $98 and push X to the stack
$CC41##Restore $98 but into X
$CC46##Restore X and A to their original values
$CFED##Although we just adjusted it for the dora, we're still resetting
\the whole wall array?
$CFEF#reset_tiles_left_loop#
$CFFE##Place address of top AI's hand into {X,Y}
$D05C#CLEAR_HAND_STRUCT#Expects pointer to hand struct to be in {X,Y}. Stops just short of
\the scores field
\
\Called from:
\ - D002
\ - D013
\ - D024
\ - D035
\
\Starts by clearing any data from the old hand to all FF (the
\sentinel value). 
$D066#clear_hand_looptop#
$D070#clear_hand_loop2top#For some reason we clear this part of the hand to 0
$D076##Aha! Number of tiles in hand is at offset $39 in hand struct
$D005##These seem to be two more members of the hand struct, but idk
\what they mean right now
$D042##We set X and Y but I'm not sure why
$D048##Takes player number in A and loads the pointer to its hand struct
\in the global HAND_PTR register {F,10}
$D04E##Increment current player for some reason?
$D04C##Set some random spot to 0 in the player struct
$D046#assign_seat_winds#
$C078##Shuffle the wall again for good measure?
$D07D#DEAL_HANDS#Called from:
\ - C07B
\
\Save $34 as next draw index. Note that $34 = 52 which is 4*13,
\the total number of dealt tiles
$D083##Load player's hand into the global hand pointer and run the
\DEAL_HAND function
$D0AC#DEAL_HAND#Expects you to write the hand_pointer in {F,10}. This will take the
\next 13 tiles from the wall and copy them into the targeted hand
\struct. Will also compute a tenpai heuristic for each hand.
\
\Called from:
\ - D08B
\ - D096
\ - D0A1
\
\Copy hand pointer into {A2,A3}
$D0B6#copy_wall_to_hand_looptop#Very elegant use of the addressing modes for the 6502
$D0BD##Do this 13 times
$D0C1##Make sure to set sentinel at end of hand tile list
$D0C5##Save the leftover 13 from the loop into A
$D0C6##And push it to the stack
$CCF7#COMPUTE_HAND_SCORE#Assumes there is a pointer to a hand in {$A2,$A3} (with $A2 as
\the LSB). This seems to also check for 13 orphans and chiitoitsu,
\although the 13 orphans check is broken :(
\
\Returns carry clear if in tenpai, else carry set. A (and $66) will
\be set to this hand's "tenpai score" which seems to be some
\heuristic for saying how close a hand is to victory. The smaller 
\the score, the closer to tenpai, and 0 means we are in tenpai.
\
\Called from:
\ - D0CA
$CE32#CHECK_13_ORPHANS_TENPAI#Leaves 1 in $9D if a pair of honours/terms was found (even if it 
\belongs to a larger triple/quad) and the count of unique
\honours/terms in $9C. Well, at least it would, if it wasn't broken.
\
\The function saves 13 - $9C - $9D into A (and $66) before 
\returning. The carry will be clear if A was zero, otherwise it will
\be set. Of course, A will always be 13 and the carry will always
\be set because this function is broken.
\
\Called from:
\ - CCF7
\ - CE32
\
\This is some field from the tmp_hand. But I don't know what it
\represents
$CE3D##Zero out $9C and $9D for some reason
$CE41##Also zero out Y
$CE42#count_distinct_hons_terms_looptop#Read the Yth element from list of terminals/honours. However,
\we don't seem to use the value we just read?
$CE4A##Huh?? We seem to be calling this with bogus input arguments 
\(A=0, X=13) but luckily this just grabs the sentinel and causes
\the function to return 0 gracefully (with carry set to indicate a
\problem)
$D0EF#SOME_FN_44#Called from:
\ - C08B
$D13C#SOME_FN_46#Called from
\ - D0F2
\
\Setting more of these global variables...
$FBDB##Push Y to the stack
$FC35##Load A from $0C
$FBE5##Save original wrptr. As usual, we can only update the mode at
\this address after we have put the rest of the write data in (this
\is so that the NMI doesn't read our stuff in a half-finished state)
$FBF4##Put in zero-terminator
$FBE6##I really wish I understood what $D and $E were for. It seems
\like they usually contain a VRAM pointer, but that's the best
\I can decipher
$D146#some_kind_of_display_update_loop#
$FC63#SOME_DISPLAY_THING#I'll come back to this some other time
$F812#CONVERT_TO_BASE_10_AND_SAVE_TO_STR#I'm speculating that this is a base 10 conversion due to repeated
\floor divisions by powers of ten. Each digit is converted to its
\tile number for the character in the pattern table.
\
\The input is expected to be in {$97,$98}, with MSB in $98
\
\Called from:
\ - D290
\
\Start by loading $3E6 into the hand ptr... but $3e6 is not the
\address of any of the player hands? It looks like it could be a
\hidden fifth hand?
$F81C##Store $EF into $11. When we convert the individual digits, we
\will add $EF + 1 to that digit so that it magically points it to the
\number tile in the pattern table
$F822##Save $FA to *HAND_PTR. $FA is the tile number for the "+"
\character in the pattern table.
$C091##Tee up the address for the current player
$C096##For some reason, save the currend hand pointer into {$3E,$3F}
$C09E##Check if we are in demo mode
$C0A6#ai_player#
$C0A2##Player number 0 is the human player (when we are not in demo
\mode)
$C0AE#human_player#
$C59C#PERFORM_AI_TURN#Draws a tile from wall, selects one to discard, then discards it.
\
\There is some logic to deal with the case when the AI wins, but
\I haven't gone through it yet. Also, the decision to call riichi must
\exist in here somewhere... and also how does the AI make sure
\to always have a yaku?
\
\Called from:
\ - C0A6
\
\For some reason, earlier code copied the hand pointer into 
\{$3E,$3F} and then we go on to copy it to {$A2, $A3}. No 
\idea why this is needed
$CFA8#DRAW_TILE#Returns drawn tile in A (and in $98). Takes care of registering
\display updates in the $700 array. Also maintains the state of
\the wall.
\
\Called from:
\ - C5A4
\
\Push X onto the stack
$D277#SOME_FN_72#This is a display-related function
\
\Called from:
\ - CFB5
\
\Start by copying {$A,$B} to the stack. I don't know what is in
\these addresses, but when I was single-stepping $A was $16
\and $B was $06
$D27D##Push X and Y to the stack
$D281##Zero out $A and $B. We probably pushed them to the stack to
\preserve their values
$FCB9#SOME_FN_73_THEN_REGISTER_VRAM_WRITE#Called from:
\ - D28D
$D289##Load some values into X and Y. Probably in preparation for the
\next function call. This address $D2CD points to a constant 
\string:
\$CD, $C5, $D3, $00
\No idea what this is
$FB1F#SOME_FN_73#Called from:
\ - FCB9
$FB21##Zero out $E
$FB23##Load the value in $B. We then do the following five times:
\- Shift A left, saving MSB in Carry register
\- Rotate $E left, putting the saved Carry bit in its LSB
\This has the effect of doing $E = ($B>3) & $1F
$FB36##Now load $A into A and clear the Carry bit
$FB34##At this point, A = $B << 5. For some reason we save it into $D
$FB39##We add whatever was in $A to $D
$FB3D##We add whatever was in $5 to $E, noting that we include the
\Carry from the previous addition between $A and $D. I have
\no idea what's going on here
$D292##Save live wall tiles left in $97
$D296##Save 0 in $98. This is because our base 10 conversion function
\expects 16b quantities and we need to clear the MSB to 0.
$F842#input_is_sanitized#Move to next tile in string (after the sign character)
$F893#DIVIDE_16B_AND_SAVE_TO_STRING#Divides two (equal-sign) 16-bit numbers. The dividend is assumed
\to be in {$97, $98} (LSB is in $97) and the divisor is in {$99,$9A}.
\The result is saved in HAND_PTR[Y++]. The dividend's original
\value is lost, but the divisor stays unchanged.
\
\Well, for some reason the divisor value is initialized with the value
\in $11, which means that the result is (in1/in2) + $11. I think this
\is so we can add $F0 to the result so it points to one of the
\decimal number tiles in the pattern table
\
\Called from:
\ - F84B
\
\As far as I can tell, this function repeatedly subtracts the 16-bit
\value {$99,$9A} from {$97,$98} until the result is negative, then
\undoes the last subtraction. It keeps a count of subtractions in X
\so maybe this is a 16-bit (very slow) divmod algorithm?
$F899##I think when this function is called, $99 always contains $10
$F89B##Save $97 - $99 (with Carry hardcoded to 1) into $97. Recall that
\for a subtractrion, we subtract ~C, so setting the Carry to 1 
\means no borrow input.
$F8A1##Save $98 - $9A (with whatver Carry is left from the last
\subtraction) into $98. In other words, this must be the upper
\16-bits of the subtraction
$F895#divide_16b_looptop#
$F8A5##Last subtraction went too far and made result negative; undo
\it. BTW Carry is guaranteed to be 0 here.
$F8B1##Take our quotient and put it into A
$F8B4##Go to next index... but the next index of what?
$F8B2##Save quotient into HAND_PTR[Y], but who knows why. When
\this function is called, HAND_PTR doesn't actually seem to 
\point at any of the players' hands (so maybe I should rename it
\to STRING_PTR or something)
$F828##Oh, very clever! Where $EF is the base address of the white
\number characters, $DF is the base of the red number characters
\and this makes the value print in red
$F82C##Save $EB (the tile ID of the "-" character" to our tile string
$F830##We will take our negative number and take its twos complement.
\This is a little annoying because it's 16-bits but the processor is
\only 8-bit.
\
\Start by inverting A (which was already loaded with $98, the
\most significant byte of the input). We will save it back to $98
\when we're done
$F836##Now we invert the LSB in $97 and add 1
$F83E##Here we test if the increment to the LSB would cause us to have
\to increment the MSB
$F840##Increment the MSB if necessary
$F843##Load the value $2710 into {$99,$9A} (LSB in $99). This is
\10000 in decimal.
$F84E##Load $3E8 into {$99, $9A}. This is 1000 in decimal.
$F859##Load $64 into {$99,$9A}. This is 100 in decimal
$F864##Load $A into {$99,$9A}. Do I need to tell you what this is in
\decimal?
$F86F##No need to do a division for the last digit, it is what is left in
\{$97,$98} after the divisions. Here we get the tile ID for thus
\digit of the number string. Importantly, we do this by checking 
\$11, which takes care of selecting red or white digits as 
\necessary
$F874##Add the number tiles base to the remainder of the division by 10
\left behind in $97
$F878##This is strange... we go back to read the first character of the
\string we've been building
$F87C##Oh, I see. We're checking if it's a leading zero, and if so we
\won't print it
$F887#no_more_leading_zeroes#Our number strings are always 8 characters long, and we have
\written 7 of them
$F880##$EA is an empty tile. Overwrite the leading zero with this.
$F884##Go to the next digit
$F87A#clear_leading_zeroes#
$F885##This branch will always be taken (Y will not be zero here). I
\think Tobita-san used a BNE instruction to save a few bytes,
\since it is a relative jump
$F889##Remember that scores are saved internally as one-tenth the
\actual value. So we need to append a trailing zero at the end
$F88B##$11 contains the tile ID for 0, in white or red as needed
$F88E##Finally, zero-terminate our tile string
$CFB8##Restore X
$C5A7##Save drawn tile into $9C
$C5AB##Recall that we saved the tile string pointer into {$3E,$3F}. This
\seems to be dealing with element 57 of that string. It seems that
\this element contains the number of concealed tiles
$D0E1#GET_SOME_HAND_METADATA#A seems to be the length of a hand of mahjong tiles, and the 
\pointer to the hand is in {$A2,$A3} (LSB in $A2). Seems to be
\saving its "return values" in $A and $B. $A will contain twice the
\length of the hand, and I don't know what $B is yet
\
\Called from:
\ - C5AD
\
\Multiply length by 2?
$D0E2##Save this in $0A for some reason
$D0E4##Save Y to the stack
$D0E6##Access element 64 from the hand. I don't know what this
\element means, currently.
$D0EC##Restore Y and return
$C5B3##Add 4 to whatever that function returned in $B. Still have no
\idea what it is...
$C5B7##Now we check $95 for something... what was in here, again?
$C5BE#skip_weird_opcode_and_a_half#
$C5BD##Whoa, this is weird... these next three bytes will run as
\LDX $9CA5
\if the value in $95 was zero... but if it wasn't zero, we actually
\jump to the middle of this opcode and run:
\LDA $9C
$C5C3##Append tile saved in $9C to current player's hand. Remember
\that we copied the hand pointer to {$3E,$3F}.
$C5BB##Restore drawn tile from $9C (it was saved here because we
\needed to use A)
$C616#SELECT_AND_DISCARD#Seems like this expects a hand pointer in {$3E,$3F}. Selects the
\best discard according to some heuristic then edits all the structs
\to make that happen. Also seems to register display updates.
\
\Called from:
\ - C5CC
\
\The first thing we do is copy the hand pointer to {$F,$10}. This
\is so we can copy it to the tmp_hand area
$F74C#SELECTION_SORT_COPY#Expects a valid hand to be inside $200. A, X, and Y will be 
\clobbered. The hand itself will not be changed, but its sorted
\version will be written to $44E.
\
\Called from:
\ - C621
\
\Start by zeroing out A, $9D, X, and Y
$F761#no_tiles_left#Write sentinel
$F759##Save smallest tile in $9D
$F752#selection_sort_looptop#
$E20C#GET_LEGAL_KAN_MOVES?#Always operates on tmp_hand. Seems like it saves a list of 
\possible moves in the list at $7E, formatted as the number of
\elements followed by the elements themselves. Seems to check
\for:
\- Calling closed kan
\- Adding to an open kan
\
\Called from:
\ - C624
\
\Zero out $7E
$E210##Get number of live wall tiles left (this should be after drawing the
\player's next tile, though maybe this function is called after a
\pon/chi/kan decision)
$E260#live_wall_empty#
$E214##Zero out A, $33, and X
$E21A##Get next tile from sorted array at $44E
$E21D##Check if we hit the sentinel
$E259#add_sentinel_and_return#
$E233#not_a_quad#This loads offset $3A from the tmp_hand, but I don't know the
\meaning of this field. Hypothesis: called sets?
$E255#try_next_tile#We are using $33 as our iteration variable to avoid conflicts on
\the X register with called functions
$E22A##If this is a quad, it seems like we set some of the upper bits of
\the first tile, then jump to some label
$E24F#add_something_to_7E_list#Seems like the format of the data structure at $7E is a length
\followed by some number of marked tile IDs
$E257##I think this is a BNE to save bytes
$E218#some_fn_78_looptop#I guess $33 is our current array index?
$E23D##kans are ORed with $C0... maybe pons are ORed with $40?
$C633#not_a_winning_hand?#Maybe this is where a discard decision is made?
\Recall {$3E,$3F} is a copy of the current hand ptr (I think)
\This accesses offset $3E in the hand, which I think is the number
\of called sets (but I'm not sure)
$C67C#one_or_more_called_sets?#
$C69A#SOME_PREDICATE_FN_80#Called from:
\ - C639
\
\I have no idea what's in $8B
$C69E##I think 7E is the list of kans and called sets, but I'm not sure.
$C6D9#no_kans_or_called_sets?#
$EBD1#COPY_TMP_HAND_AND_CALLED_SETS_TO_45D#A, X, and Y get clobbered
\
\Called from:
\ - C63E
\
\Zero out A, X, and Y
$EBD5#cpy_tmp_hand_to_45D#Load tmp_hand[X]
$EBE4#done_copying_tmp_hand_to_45D#
$EC2E#add_sentinel_to_45D#
$C90B#GEN_CUR_PLAYER_TILES_LEFT_PER_TYPE#The global tiles_left_per_type is only affected by discards, called
\sets, and dora indicators. However, each player has their own 
\private information (i.e. their concealed tiles) and can create 
\their own better idea
\
\Called from:
\ - C644
$C854#SOME_PREDICATE_FN_82#Seems like it might be checking for tenpai? Or maybe the presence
\of a yaku?
\
\Expects {$A2, $A3} to point to current player's hand
\
\Called from:
\ - C641
\
\Seems to be loading some field member from the TMP_HAND, at
\offset $31, but idk what this is.
$C86F#need_further_checks#This is loading some other member from the tmp_hand struct,
\but idk what it is. But if it's strictly less than 6 we return with
\carry clear.
$C04F#wait_for_ppu_again#
$C070##If I had to guess, the carry is 1 for demo mode, and 0 for player
\mode
$D081##X will be our index into the wall as we deal each player hand (i.e.
\copy into the player structs)
$CE35##If that field form the struct (I am only 20% sure it is the number
\of called sets) is zero, then we skip the next two instructions.
$CE3B#hand_struct_field_was_zero#
$CE39##I guess this uses BNE to save a few bytes over an unconditional
\jump
$CE65#save_A_to_66_and_return_it#
$CE67##Basically, this returns with carry set when A is nonzero, otherwise
\it returns with carry clear
$CE5D#count_distinct_hons_terms_loopdone#A = 13 - $9C - $9D. A will be 0 if and only if this hand is a 13
\orphans hand. (Or at least it would if this function worked)
$F0BE#TERMS_HONOURS_STR[0]#
$F0BF#TERMS_HONOURS_STR[1]#
$F0C0#TERMS_HONOURS_STR[2]#
$F0C1#TERMS_HONOURS_STR[3]#
$F0C2#TERMS_HONOURS_STR[4]#
$F0C3#TERMS_HONOURS_STR[5]#
$F0C4#TERMS_HONOURS_STR[6]#
$F0C5#TERMS_HONOURS_STR[7]#
$F0C6#TERMS_HONOURS_STR[8]#
$F0C7#TERMS_HONOURS_STR[9]#
$F0C8#TERMS_HONOURS_STR[A]#
$F0C9#TERMS_HONOURS_STR[B]#
$F0CA#TERMS_HONOURS_STR[C]#
$F0CB#TERMS_HONOURS_STR[D]#
$CE45##This branch is taken when we read the zero-terminator from that
\constant string in ROM
$CE48##Discard the value we just read? Theory: this instruction used to
\be LDX $00, but it was patched because of some bug.
$CE59#increment_unique_term_hon_count#I'll bet this function was originally made to count distinct terminals
\and honours, most likely for that rule about a draw when you 
\start with 9 or more of them.
$CE51##If we found exactly one of this term/hon tile, then we just
\increment $9C. If we found more than one, we write 1 into $9D.
$CE4D##If we didn't find that honour/terminal, don't increment anything
\and just go to the next one. I guess this function isn't just for
\13 orphans.
$CD0F#found_tenpai#
$CCFC##Write A to $66 (for the second time... CHECK_13_ORPHANS
\already does this before returning)
$CCFE##Get offset $31 from the given hand. I don't know the meaning
\of it though... but if its bit 3 is set, apparently it means we are
\in tenpai.
$CE08#CHECK_CHIITOITSU_TENPAI#Assumes that this has been called after FIND_IDENTICAL_TILE_
\GROUPS. In other words, expects the number of groups to be
\in $64 and the groups themselves to be in $446 (with sentinel as
\terminator).
\
\Returns carry clear if in tenpai, carry set otherwise
\
\Called from:
\ - CD0A
\
\Check for 6 pairs. If so, does a further check, probably to see if
\there is a triple (chiitoitsu is usually not allowed to contain two
\identical pairs)
$CE11#check_no_triples#For each element in the list of groups, count how many of each
\there are and return with carry set if there is a triple
$CE13#check_no_triples_looptop#
$CE26#check_no_triples_loopdone#It seems that these tenpai functions also like to leave the return
\value in $66?
$CE2C#too_bad_you_are_not_in_tenpai#
$CD10#didnt_find_tenpai_yet#
$CD85#SOME_FN_411#Theory: if A is less than the last function return value (saved
\in $66) then we save A to $98, which is commonly used as a
\backup of A. I really don't know what this is used for.
\
\Called from:
\ - CD10
$D0D4##This seems to be checking for that rule about a draw at the start
\if someone has 9 terminals/honours or more
$CD13##Save 1 in $63
$CD64#pair_candidate_tenpai_check_loopdone#At this point, we were not able to find tenpai by assuming that
\the hand already had a pair. So now we will see if maybe it has
\a pair-wait tenpai
\
\It seems like $63 is used as the "number of pairs found so far".
\Before this point, we call PULLDOWN_PARTIAL_GROUPS with a
\pair candidate, but here we aren't.
$CD20##If we are here, it means there is at least one identical tile group.
\A currently contains the tile ID for the first group, and we save 
\it in $9C in preparation for calling FIND_TILE
$CD22##Push X+1 onto the stack. X is the index of the current pair
\candidate, but we need to save it so that we can use X in the
\loop body.
$CD25##Set X to 0 to prepare FIND_TILE
$CD2A##Swap tmp_hand[X] with tmp_hand[Y].X is always zero here.
\This is to pull odwn a pair whose tile ID is equal to the one we
\just got from the list of identical tile groups.
$CD38##X should always be one after this instruction. This searches the
\remaining array for the next instance of the tile in this group
$CD27##Recall FIND_TILE leaves index of found tile in Y. By the way,
\we're guaranteed to find this tile because this was in our list
\of groups that we previously made on this very same hand.
$CD3C##Swap tmp_hand[X] and tmp_hand[Y]
$CD4A##Increment X past the pair we pulled down. It should always be 2
\here.
$CD8C#PULLDOWN_PARTIAL_GROUPS_PON_PRIORITY#Takes X as the start index in tmp_hand. Returns with carry clear
\if and only if hand was in tenpai. Assumes $63 is number of pairs
\already found before X. Will check the number of called sets from
\the tmp_hand struct.
\
\After this function, $62 will contain the number of complete 
\sequences in the tmp_hand (including any called sets), $64 will
\contain the number of pairs (including whatever we had in $63),
\and $65 will contain the number of partial sequences.
\
\Amazing! I think this can work for an input like 233444556 because
\it tries to group the set first with pon priority, then it tries again
\with sequence priority.
\
\Called from:
\ - CD4D
$F5FF#INIT_GROUP_COUNTS#Initializes group counts. $63 is set by the calling code to be the
\number of pairs already found in the hand before X. This function
\will allso read the number of called sets from the tmp_hand struct.
\It will puts the number of called sets in $62, the number of pairs
\in $64, and 0 in $65.
\
\Called from:
\ - CD8C
$CDDB#PULLDOWN_PONS_KANS#Expects X to be an index into the tmp_hand array. Tries to pull
\down all pons/kans to X. Saves count of pons/kans in $62.
\
\Similar to PULLDOWN_GROUPS (the subroutine at address 
\E49C), except does not look for sequences and does not leave
\tiles in increasing order. This makes it run faster, I guess.
\
\Algorithm: Y is the current "trial tile", X is the place to start from.
\For a given X, iterate through all Y gte X looking for pons/kans,
\moving them down to X,X+1,X+2 (and incrementing X) as they
\are found. Does this until the remaining tiles do not form any
\pons/kans
\
\Called from:
\ - CD8F
\
\Copy X to Y
$CDDD#some_fn_4122_looptop#
$CDEF#some_fn_4122_done#
$CDE0##Quit fn if we hit the sentinel
$CDEA##Hmmm... this $62... is it a cached number of groups?
$CDE8##If no set found, just move on to the next tile. Otherwise, we'll
\increment $62, then copy our updated X into Y. Very elegant.
$CDF0#PULLDOWN_SEQUENCES#Tries to pull down as many sequences as possible. Like
\PULLDOWN_PONS_KANS but for sequences.
\
\This is a tail-recursive function
\
\Called from:
\ - CD92
\
\$9E will contain our trial tile ID, and here we initialize it to 0 to that
\the first call to FIND_SMALLEST_ABOVE_A finds the smallest tile
\ID in the whole hand
$CDF6##Get next smallest tile.
$CE07#pulldown_sequences_loopdone#
$CDF4#pulldown_sequences_looptop#Restore A from $9E
$CDFB##Save tile ID in $9E so we don't clobber it
$CE02##Seems like $62 is a count of complete groups
$CE00##If no set found, just move on to the next tile. Otherwise, we'll
\increment $62, then reset A to 0. This is classic tail recursion.
$F60D#PULLDOWN_PARTIAL_GROUPS#Treats X as start index in tmp_hand. Pulls down all pairs and 
\adds their count to $64. Pulls down all sequences (or partial
\sequences of two elements) and adds their count to $65, though
\I'm not sure about the partial sequences yet...
\
\Called from:
\ - CD95
\
\Copy X to Y
$F60F#pulldown_pairs_looptop#In this loop, Y is the "trial tile". We iterate over all Y gte X to 
\see if we can pull down a pair
\
\Get tmp_hand[X]
$F620#pulldown_pairs_loopdone#Initialize $9E to zero for this next loop
$F61A##If no set found, just move on to the next tile. Otherwise, we'll
\increment $64, then copy our updated X into Y. Very elegant.
$F559#PULLDOWN_SINGLE_PAIR#Expects a tile number in A, and X to be start index. Pulls down
\pair of A tiles (if possible) to indices X,X+1 of the tmp_hand.
\Returns with carry clear if pair found, else carry set
\
\Called from:
\ - F617
\
\Save A to $98
$F55B##Save $9C on the stack so it doesn't get clobbered
$F55E##Restore A
$F560##Save input to $9C because that is where COUNT_IDENTICAL_TILES
\expects its input to be. btw X is current start index
$F565##If there were two or more of this tile, go on to check some
\other thing. Otherwise, restore original $9C, set carry, and
\return
$F56E#two_or_more_of_this_tile#Save Y to the stack, because FIND_TILE would clobber it with its
\return value
$F573##Pull down found tile
$F594##Restore original Y and $9C, then return with carry clear
$F624#pulldown_partial_sequences_looptop#By the time this loop starts, X points past all complete pons/kans,
\complete sequences, and pairs
\
\Restore A from $9E
$F636#pulldown_partial_sequences_loopdone#
$F62B##Save A in $9E for the next iteration of this loop
$F59B#PULLDOWN_PARTIAL_SEQUENCE#Expects a tile number in A and a start index in X. Will attempt to
\pull down A to X and A+1 to X+1, returning with carry set if it
\didn't work. If it did, X will be incremented.
\
\Is almost identical to PULLDOWN_SINGLE_SEQUENCE at
\address F483, but will only look for two sequential elements
\
\Called from:
\ - F629
\
\Check if honour tile
$F5FD#return_with_carry_set_partseq#
$F59F##Save A in $98, then push $9C, Y, and X to the stack. This is so
\we can restore them at the end of the function.
$CDA7#check_tenpai_after_partial_grouping#Recall: $62 is number of complete sets, $64 is number of pairs,
\and $65 is number of partial sequences. These are the totals of
\whatever this function found plus whatever was found before
\this function was called.
\
\Start by calculating $62 + $64 + $65, which is the total number
\of sets (complete or partial)
$CD9B#PULLDOWN_PARTIAL_GROUPS_SEQUENCE_PRIORITY#
$CDC5#check_tenpai_met#For legitiblity, in this comment I assign the names C := $62, 
\P := $64, an S := $65. This code computes 8 - (2*C + P + S).
\
\CASE 1: hand originally had 4 groups or fewer (C+P+S < 5)
\
\The only way to have tenpai is if we have four complete sets. If
\C was less than 4, then it's impossible for 2*C+P+S to be 8 given
\that (C+P+S)<5.
\
\CASE 2: hand had 5 or more sets (original C+P+S >= 5)
\
\This calculation checks for (C==3) && (P>0) && (P+S-1>=1)
\meaning we have three complete sets, at least one pair, and at
\least one partial group aside from the pair. We start by knowing
\C+P+S>=5. So, (P+S-1)>= (4-C), which is where the 4-C comes
\from at address $CDB2. The trick here is that 2*C+(P>1)+(4-C)
\can never make it to 8 if C<3.
\
\To put it all together, if C was 4, then P=S=0, and the sum is 8.
\If C is 3 and we have 5 groups or more, 2*C is 6, (4-C) is 1, and
\the sum is 8 only if we have at least one pair. If C is less than 3,
\it is impossible for the sum to
$CDD9#return_with_set_carry#
$CDB2##If we got here it's because there were 5 or more partial sets.
\
\Compute 4 - num_complete_sets
$CDB7##Save that in $65. My guess is that this is "the number of sets you
\need to complete to have a full hand"
$CDB9##Here we do $64 = min($64, 1)
$CDD3##Save this calculation in $66. Seems like $66 is used as a return
\value from certain functions
$CD7F#clear_stack_and_return_carry_clear#
$CD8B#dont_overwrite_98#
$CD55##If we're here it's because pon priority did not find tenpai, so we
\will roll back X and try with sequence priority
$CD4B##Save X to the stack. The next function call can increase X if it
\finds any groups (complete or partial) but I think we want to roll
\back X if we didn't find tenpai so we can try grouping the hand
\differently.
$CD50##If tenpai found, then we're done
$F5AA##Now put A in $9C so we can find in the hand (after X)
$F5F6#restore_and_exit_with_carry_set_partseq#Restore X, Y, and $9C, then exist with carry set
$F5B1##Swap tmp_hand[X] and tmp_hand[Y]. In other words, pull down
\the tile we just found
$F5BF##Move to next index
$F5C0##Search for sequentially-next tile
$F5C7##Pull it down
$F5D5##Go to next index
$F5D6##This looks like an unconditional jump, because I don't think X will
\ever be 0 here. I guess it's a BNE to save some bytes.
$F5EE#found_partial_sequence#Restore everything and return with carry clear
$F5D8##Weird.... there is code here to find a third member of the 
\sequence, but I think it's always skipped. I guess Tonita-san
\wanted to save dev time by copy-paste-tweak
$F632##Increment $65 with number of partial sequences
$F634##This jump should be unconditional ($65 will never be 0) so it's a
\BNE to save some bytes
$CD5F##Restore X+1 from earlier. This X is the iteration variable on the 
\"outer" for loop starting at $CD19, and indexes into the list of
\identical tile groups (i.e. pair candidates).
$CD19#try_pair_candidate_tenpai_check#Read next tile ID from list of identical tile groups in $446. We will
\use that tile ID as a pair candidate and see if we can make tenpai
$CD68##Zero out X as well
$CD6E##I still have no idea what this function is for... it might save A to
\$98, but that seems to be useless...
$CD69##Try with pon priority. If that doesn't work, we'll try again with
\sequence priority. This is to catch corner cases like 233444556.
$CDAE##If we have fewer than 5 groups, go straight to the 
\check_tenpai_met label. Otehrwise, we will futz the group counts
\to be "compatible" with the logic after that label
$CDC0##Restore flags from after first loading $64. If nonzero, we will set
\$64 to 1 before going to check_tenpai_met
$CDD7##All in all, this fancy logic took 25 instructions. A simpler (and 
\possibly equivalent) set of instructions might be:
\LDA $03
\CMP $62
\BCC return_false ; C < 3
\BEQ keep_checking ; C == 3
\BCS return true ; C == 4
\keep_checking:
\LDA $64
\BEQ return false ; No pairs
\CLC
\ADC $65
\CMP $02
\BNE return false
\return_true:
\CLC
\RTS
\return_false:
\SEC
\RTS
\
\There were probably very good reasons for why Tobita-san did it
\this way. Maybe to improve cycle count. Or maybe my logic is not
\completely equivalent.
$CD7D##No tenpai :(
$D0CD##Aha... it seems offset $42 stores the current "tenpai heuristic",
\i.e. the return value from CHECK_TENPAI. If this is 8 the hand
\is in tenpai. If it is less than 8, I don't know what it means.
$D0DE#no_9term_draw#Restore X and return
$D08E##Load top player's hand into the global hand pointer and run the
\DEAL_HAND function
$D099##Load topmid player's hand into the global hand pointer and run the
\DEAL_HAND function
$D0A4##Load botmid player's hand into the global hand pointer and run the
\DEAL_HAND function
$C07E##Load player's hand into {X,Y}
$F7A5#done_sorting#Restore X and Y
$F7B0##In this loop, $97 is the current index. We start it at $FF here 
\because the loop increments it at the start (so we want it to roll
\over to 0 on the first iteration)
$F7BC##If we make it to the end of the array, go to the next loop 
\iteration. By the way, this can happen if we made no swaps,
\and that indicates that the array was sorted
$F7C0##Put Y back to cur_idx. This is because we need (*ptr)[Y] indexing,
\which is only available with the Y register
$F7D0##Make $9A nonzero to indicate that we performed a swap, and that
\the hand may still require another sorting pass
$E238##My guess: this is checking for the case where you can add the
\drawn tile to an open kan
$E243##Wow, very clever! By marking the type of called set with the
\upper bits, we can just use FIND_TILE to ask if any of the called
\sets is a pon with the given tile ID
$E241##Aha! Looks like offset $0F is the called sets
$E248##Reload the unmodified tile[cur_idx] and fall-through to the logic
\for appending to the list of moves
$ED71##As soon as we find a single simple, just return with carry set
$EEAB##This reuses the PULLDOWN_PARTIAL_GROUPS function to count
\the number of pairs, I guess...
$EED2#no_yaku?#Zero out X and Y
$EEB4##Weird... we also jump to that label if there are any partial
\sequences...
$EED6#count_honours_terms#This iterates with X and leaves count in Y
\
\Load tile[X] from sorted hand in $44E
$EEE6#count_honours_terms_loopdone#Check if we have 9 or more honours/terminals
$C539##Looks like you can freeze the AI by holding a button?
$EBE9##Backup A in $9D
$EBED##Get next called set tile
$EBF1##Save this tile ID into $9C. We need to use A for some stuff, and
\it's faster to re-retrieve this value from zero page versus using 
\the indexed access mode
$EBF3##Check if this is a pon or kan. Called sets mark the type of group
\by setting some flags in the upper bits of the tile ID
$EC12#called_set_was_pon_or_kan#Save X to the stack
$EBF7#called_set_was_chi#Get the tile ID without any flag bits
$EBFB##Append this to our hand copy
$EBFF##Increment tile ID and append to our hand copy
$EC07##One last time: increment tile ID and append to hand copy
$EC2A#go_to_next_called_set#Recall: we saved the hand's called sets into $9D at the start of
\this function
$EC14##X will contain number of copies of tile to place in the copied hand
$EC1D#dont_set_x_to_4#
$EC16##Check if this is a pon. Kans are tagged by setting upper nybble
\to 0xC, pons are tagged by setting upper nybble to 0x4
$EC21#append_x_copies_of_tile_to_copied_hand#
$EC28##Restore X from stack
$C86E#some_fn_82_return#
$C861##Interesting! If the number of simples is lte 4 then this must be
\a half-flush
$C865##Interesting... if we have detected the presence of a half-flush
\(or an all terms/honours hand) then we set some value into the
\hand struct. This is probably caching the calculation to avoid
\redoing it later
$C90D#copy_global_tiles_left_per_type_to_4CF#By the way, there is a bug: the global tiles left don't take into
\account the first dora indicator
$C91A#factor_in_players_concealed_tiles#
$C6DA#CALCULATE_DISCARD_HEURISTICS#Populates the array at $50F with a discard heuristic for each tile
\ID in this hand. The discard heuristic seems to start at 30, and
\gets bigger to indicate a tile is a better discard choice. The
\formula is something like (30+tenpai_score+tiles_left[id]+
\dora_score[id]+chi_heuristic[id]), where tenpai_score is some 
\heuristic that counts complete and partial groups in the hand 
\with the given tile removed.
\
\Actually, that constant 30 is copied in from another array at the
\start of the AI logic, and this leads me to believe it won't always
\be 30 (otherwise, we wouldn't copy the array, we would just
\start with a constant 30). Still a few mysteries here.
\
\Set X and $4A to 0
$C6E0#copy_54F_thru_58F_to_50F#
$C6EB##Zero out $33. Seems like we're keeping an iteration variable in here
$C6EF##Save first tile of (serialized) hand into $9C, probably this is 
\initialization for a loop
$C6F4##Also save it in $4B for some reason
$F67F##Save $98 and Y so we can restore them later
$F684##Initialize Y to $FF. In the upcoming loop we increment Y at the top
\so this will make it 0 on the first iteration.
$F687##Get hand[Y]
$F6A9#tile_not_found#Restore Y and $98 before returning
$F686#find_tile_with_given_id#
$F691##If we find this tile ID, put its index (Y( into $98 (I guess we don't
\need the tile ID in $98 anymore)
$F69A##Point Y to last valid tile in hand
$F69B##Set hand[cur_index] to hand[end-1]
$F693#find_end_of_hand#
$F6A3##Overwrite hand[end-1] with sentinel
$C6FA##Whoa... are we just gonna brute force try deleting each tile to
\see hwo good the remaining hand is?
$C6FF##Very subtle. On the first iteration we don't start at the top of
\the loop. A comment lower down explains this is more detail
$C71C#discard_heuristic_loop_spot_after_deleting_tile#Save $33 and $9C to the stack because COMPUTE_HAND_SCORE
\might clobber them
$C725##Restore $9C and $33
$C722##The carry set/clear status is ignored here, but later we will use
\the value in $66, which is set by this call to CHECK_TENPAI
$C737#nope_not_in_tenpai#
$C72B##Looks like for each discard candidate we save its associated
\tenpai score in the array at $58F
$C753#done_trying_all_discard_candidates#Earlier we deleted this tile from the tmp_hand to check its
\tenpai score. Now we put it back so we can check the next 
\discard candidate
$C73B##We seem to be calculating some kind of heuristic here. We take
\the "tenpai score", multiply it by 4, then add the number of tiles
\left that are equal to the discard candidate. Seems like smaller
\heuristic is better. I'm not sure why the heuristics seem to start
\at 1E...
$C744##I guess this is some further discard heuristic. We check how many
\tiles of the discarded tile are left and also add it in. I suppose the
\reasoning is that the tile is more likely to be useful to other
\players if there are many left
$C701#discard_heuristic_for_each_candidate#Get next tile from hand. We will delete it and then compute the
\discard heuristic, and then put it back into the hand
$C713##This is subtle... seems like instead of calling DELETE_TILE for
\each discard candidate we'll actually just overwrite the the 
\current discard candidate with the last tile we deleted. This is
\why we jump to the middle of loop on the first iteration
$C70A##Update our iteration variable
$C70C##Now search for the current tile in the array so we can delete it
$C75C##Looks like here we check for doras and we'll probably adjust the
\discard heuristics in consequence
$C75E##idk what $35 is
$C760#Save $35 in $9D (for some reason)#
$C762##This is the dora tile
$F767#DORA_WRAPAROUND#Expects a tile ID in A, which has just been incremented for the
\sake of calculating a dora tile. This might cause A to be an invalid
\tile ID, but this function will wrap it around to the correct value.
\
\Called from:
\ - C766
$F795#some_fn_90_return#
$F76B##Save A in $98, probably to prevent clobbering
$F76D##Check if honour tile
$F77F#tile_is_honour#Check if invalid wind
$F787#wind_corrected#Check if invalid dragon tile... except shouldn't this be $38?
$F783##Wrap around to East
$F785##Looks like an unconditional jump, but written as BNE to save bytes
$F78F#dragon_corrected#weird... 
$F78B##Wrap around to green
$F793#tile_corrected#
$C769##Put dora tile in X
$C76A##Dora increases heuristic by 2. 
$C773##Intersting. Decrement the discard heuristic (i.e. declare a tile to
\be a better discard) if it is equal to the dora indicator and if it is
\a suit tile. I guess this is to discourage sequences depending on
\this less-likely tile
$C777##This may lead to an invalid tile ID but it's OK because there are
\"'buffer entries" around valid tile ID indexes.
$C77B##We also discourage keeping the dora indicator and the tile after
\it too (because that sequence could end up depending on the
\dora indicator)
$C765##Interesting, the AI seems to be cheating here and is able to look
\at a hidden wall tile next to the dora indicator
$C770##Cheater! This tile isn't always visible!
$C783##Y is always 2 at this point, though we'll just overwrite it before
\reading from it again
$C784##$9D was initialized to whatever was in $35 before this function
\was called. I have no idea what either mean, or why we're
\decrementing $9D here.
$C788##Now we zero out $32, but I have no idea what that means
$C78C#chi_discard_heuristic#Looks like $32 is our iteration variable
$C78E##Get hand[Y]
$C7D7#chi_discard_heuristic_loopdone#
$C795##Increment our iteration variable
$C797##Cache current tile to $9C to speed up future reads
$C799##...but also save it in X so we can access the heuristic array
$C79A##Wait.... what is this about? Add 2 to the discard heuristic for this
\tile?
$C8DB#YAKU_DISCARD_HEURISTIC?#Called from:
\ - C7A0
$C90A#no_yaku_flags?#
$C7A3##Save A to $9C
$C7B5##For each tile in the hand we will decrease the discard heuristic for
\tiles that could form chis with it. We pre-emptively decrement the
\heuristic for this tile only to reincrement it later.
$C7B7##I tihnk this is meant to stop us from going past the end, but I
\don't think checkign for -ve works for Pin and Sou suits
$C7D5##Looks like an unconditional jump, but with BNE to save bytes
$C537#WAIT_FOR_NO_INPUT?#Called from:
\ - C64E
$C651##I have no idea what $4A is
$C655##but if it's less than 6 we... do something else...
$C810#GET_LIST_OF_BEST_DISCARDS#Iterates through the uniquified+sorted hand in $44E to get the
\tile IDs in the current hand. For each one it will look up the
\discard heuristic in the array at $50F. It will return a list of the tile
\IDs that have the highest discard heuristic in a list at $05D0, 
\formatted as {length, elements...}
\
\I think the reasoning is that as we scan through the array, when
\we find a strictly greater discard heuristic, we clear the list and
\add the associated tile ID, but if we find an equal heuristic we 
\will instead append it to the list. 
\
\Called from:
\ - C670
\
\Set $9D to $FF
$C814##Set $33 and X to 0
$C818#find_max_heuristic_looptop#$33 is our iteration variable. Load the next tile from our sorted
\list of tiles
$C81D##increment iteration variable
$C841#find_max_heuristic_loopdone#
$C823##Save current tile in $9C and X
$C826##Get the discard heuristic for this tile
$C829##Compare it to $9D, which holds the current largest discard
\heuristic
$C82F#reset_best_discards_list#The current discard heuristic is larger than the one in $9D. Save
\it as the current max
$C831##Zero out $05CF for some reason
$C836#append_to_best_discards_list#Re-read current tile
$C838##Append it to the list
$C83B##Y will stay the same until we find a strictly larger heuristic, then
\we will reset it back to zero
$C83C##Increment the length of this list
$C83F##This looks like an unconditional jump that uses BNE to save bytes
$C84C#select_random_discard#
$F6CC#SOME_FN_92#Expects a hand pointer in {X,Y} and a tile ID in A
\
\Save A, and set the hand_ptr.
$C1DC#someone_won?#
