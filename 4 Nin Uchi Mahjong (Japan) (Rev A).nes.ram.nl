$0019#INPUT#The subroutine at FE86 shifts in the controller values on every frame.
\The format is:
\- D0: A
\- D1: B
\- D2: Select
\- D3: Start
\- D4: Up
\- D5: Down
\- D6: Left
\- D7: Right
$001A#AB_PRESSED#
$0004#UPDATE_SPRITES#When 1, on the next NMI the data in $600-$6FF will be DMAed
\to OAM (a.k.a. sprite memory)
$0003#SKIP_FRAME?#
$0002#THINGS_TO_DO?#
$0700#NAME_TABLE_UPDATES[0]#Still trying to figure out the format. Seems like it might be:
\- Some kind of mode. If 0, does nothing. If 4, does as shown below
\- Hi 8 bits of address into VRAM
\- Lo 8 bits of address into VRAM
\- Zero-terminated string of tile numbers
$0701#SOME_ARRAY[1]#
$0702#SOME_ARRAY[2]#
$0703#SOME_ARRAY[3]#
$0704#SOME_ARRAY[4]#
$0705#SOME_ARRAY[5]#
$0706#SOME_ARRAY[6]#
$0707#SOME_ARRAY[7]#
$0708#SOME_ARRAY[8]#
$0709#SOME_ARRAY[9]#
$070A#SOME_ARRAY[A]#
$070B#SOME_ARRAY[B]#
$070C#SOME_ARRAY[C]#
$070D#SOME_ARRAY[D]#
$070E#SOME_ARRAY[E]#
$070F#SOME_ARRAY[F]#
$0710#SOME_ARRAY[10]#
$0711#SOME_ARRAY[11]#
$0712#SOME_ARRAY[12]#
$0713#SOME_ARRAY[13]#
$0714#SOME_ARRAY[14]#
$0715#SOME_ARRAY[15]#
$0716#SOME_ARRAY[16]#
$0717#SOME_ARRAY[17]#
$0718#SOME_ARRAY[18]#
$0719#SOME_ARRAY[19]#
$071A#SOME_ARRAY[1A]#
$071B#SOME_ARRAY[1B]#
$071C#SOME_ARRAY[1C]#
$071D#SOME_ARRAY[1D]#
$071E#SOME_ARRAY[1E]#
$071F#SOME_ARRAY[1F]#
$0720#SOME_ARRAY[20]#
$0721#SOME_ARRAY[21]#
$0722#SOME_ARRAY[22]#
$0723#SOME_ARRAY[23]#
$0724#SOME_ARRAY[24]#
$0725#SOME_ARRAY[25]#
$0726#SOME_ARRAY[26]#
$0727#SOME_ARRAY[27]#
$0728#SOME_ARRAY[28]#
$0729#SOME_ARRAY[29]#
$072A#SOME_ARRAY[2A]#
$072B#SOME_ARRAY[2B]#
$072C#SOME_ARRAY[2C]#
$072D#SOME_ARRAY[2D]#
$072E#SOME_ARRAY[2E]#
$072F#SOME_ARRAY[2F]#
$0730#SOME_ARRAY[30]#
$0731#SOME_ARRAY[31]#
$0732#SOME_ARRAY[32]#
$0733#SOME_ARRAY[33]#
$0734#SOME_ARRAY[34]#
$0735#SOME_ARRAY[35]#
$0736#SOME_ARRAY[36]#
$0737#SOME_ARRAY[37]#
$0738#SOME_ARRAY[38]#
$0739#SOME_ARRAY[39]#
$073A#SOME_ARRAY[3A]#
$073B#SOME_ARRAY[3B]#
$073C#SOME_ARRAY[3C]#
$073D#SOME_ARRAY[3D]#
$073E#SOME_ARRAY[3E]#
$073F#SOME_ARRAY[3F]#
$0740#SOME_ARRAY[40]#
$0741#SOME_ARRAY[41]#
$0742#SOME_ARRAY[42]#
$0743#SOME_ARRAY[43]#
$0744#SOME_ARRAY[44]#
$0745#SOME_ARRAY[45]#
$0746#SOME_ARRAY[46]#
$0747#SOME_ARRAY[47]#
$0748#SOME_ARRAY[48]#
$0749#SOME_ARRAY[49]#
$074A#SOME_ARRAY[4A]#
$074B#SOME_ARRAY[4B]#
$074C#SOME_ARRAY[4C]#
$074D#SOME_ARRAY[4D]#
$074E#SOME_ARRAY[4E]#
$074F#SOME_ARRAY[4F]#
$0750#SOME_ARRAY[50]#
$0751#SOME_ARRAY[51]#
$0752#SOME_ARRAY[52]#
$0753#SOME_ARRAY[53]#
$0754#SOME_ARRAY[54]#
$0755#SOME_ARRAY[55]#
$0756#SOME_ARRAY[56]#
$0757#SOME_ARRAY[57]#
$0758#SOME_ARRAY[58]#
$0759#SOME_ARRAY[59]#
$075A#SOME_ARRAY[5A]#
$075B#SOME_ARRAY[5B]#
$075C#SOME_ARRAY[5C]#
$075D#SOME_ARRAY[5D]#
$075E#SOME_ARRAY[5E]#
$075F#SOME_ARRAY[5F]#
$0760#SOME_ARRAY[60]#
$0761#SOME_ARRAY[61]#
$0762#SOME_ARRAY[62]#
$0763#SOME_ARRAY[63]#
$0764#SOME_ARRAY[64]#
$0765#SOME_ARRAY[65]#
$0766#SOME_ARRAY[66]#
$0767#SOME_ARRAY[67]#
$0768#SOME_ARRAY[68]#
$0769#SOME_ARRAY[69]#
$076A#SOME_ARRAY[6A]#
$076B#SOME_ARRAY[6B]#
$076C#SOME_ARRAY[6C]#
$076D#SOME_ARRAY[6D]#
$076E#SOME_ARRAY[6E]#
$076F#SOME_ARRAY[6F]#
$0770#SOME_ARRAY[70]#
$0771#SOME_ARRAY[71]#
$0772#SOME_ARRAY[72]#
$0773#SOME_ARRAY[73]#
$0774#SOME_ARRAY[74]#
$0775#SOME_ARRAY[75]#
$0776#SOME_ARRAY[76]#
$0777#SOME_ARRAY[77]#
$0778#SOME_ARRAY[78]#
$0779#SOME_ARRAY[79]#
$077A#SOME_ARRAY[7A]#
$077B#SOME_ARRAY[7B]#
$077C#SOME_ARRAY[7C]#
$077D#SOME_ARRAY[7D]#
$077E#SOME_ARRAY[7E]#
$077F#SOME_ARRAY[7F]#
$0780#SOME_ARRAY[80]#
$0781#SOME_ARRAY[81]#
$0782#SOME_ARRAY[82]#
$0783#SOME_ARRAY[83]#
$0784#SOME_ARRAY[84]#
$0785#SOME_ARRAY[85]#
$0786#SOME_ARRAY[86]#
$0787#SOME_ARRAY[87]#
$0788#SOME_ARRAY[88]#
$0789#SOME_ARRAY[89]#
$078A#SOME_ARRAY[8A]#
$078B#SOME_ARRAY[8B]#
$078C#SOME_ARRAY[8C]#
$078D#SOME_ARRAY[8D]#
$078E#SOME_ARRAY[8E]#
$078F#SOME_ARRAY[8F]#
$0790#SOME_ARRAY[90]#
$0791#SOME_ARRAY[91]#
$0792#SOME_ARRAY[92]#
$0793#SOME_ARRAY[93]#
$0794#SOME_ARRAY[94]#
$0795#SOME_ARRAY[95]#
$0796#SOME_ARRAY[96]#
$0797#SOME_ARRAY[97]#
$0798#SOME_ARRAY[98]#
$0799#SOME_ARRAY[99]#
$079A#SOME_ARRAY[9A]#
$079B#SOME_ARRAY[9B]#
$079C#SOME_ARRAY[9C]#
$079D#SOME_ARRAY[9D]#
$079E#SOME_ARRAY[9E]#
$079F#SOME_ARRAY[9F]#
$07A0#SOME_ARRAY[A0]#
$07A1#SOME_ARRAY[A1]#
$07A2#SOME_ARRAY[A2]#
$07A3#SOME_ARRAY[A3]#
$07A4#SOME_ARRAY[A4]#
$07A5#SOME_ARRAY[A5]#
$07A6#SOME_ARRAY[A6]#
$07A7#SOME_ARRAY[A7]#
$07A8#SOME_ARRAY[A8]#
$07A9#SOME_ARRAY[A9]#
$07AA#SOME_ARRAY[AA]#
$07AB#SOME_ARRAY[AB]#
$07AC#SOME_ARRAY[AC]#
$07AD#SOME_ARRAY[AD]#
$07AE#SOME_ARRAY[AE]#
$07AF#SOME_ARRAY[AF]#
$07B0#SOME_ARRAY[B0]#
$07B1#SOME_ARRAY[B1]#
$07B2#SOME_ARRAY[B2]#
$07B3#SOME_ARRAY[B3]#
$07B4#SOME_ARRAY[B4]#
$07B5#SOME_ARRAY[B5]#
$07B6#SOME_ARRAY[B6]#
$07B7#SOME_ARRAY[B7]#
$07B8#SOME_ARRAY[B8]#
$07B9#SOME_ARRAY[B9]#
$07BA#SOME_ARRAY[BA]#
$07BB#SOME_ARRAY[BB]#
$07BC#SOME_ARRAY[BC]#
$07BD#SOME_ARRAY[BD]#
$07BE#SOME_ARRAY[BE]#
$07BF#SOME_ARRAY[BF]#
$07C0#SOME_ARRAY[C0]#
$07C1#SOME_ARRAY[C1]#
$07C2#SOME_ARRAY[C2]#
$07C3#SOME_ARRAY[C3]#
$07C4#SOME_ARRAY[C4]#
$07C5#SOME_ARRAY[C5]#
$07C6#SOME_ARRAY[C6]#
$07C7#SOME_ARRAY[C7]#
$07C8#SOME_ARRAY[C8]#
$07C9#SOME_ARRAY[C9]#
$07CA#SOME_ARRAY[CA]#
$07CB#SOME_ARRAY[CB]#
$07CC#SOME_ARRAY[CC]#
$07CD#SOME_ARRAY[CD]#
$07CE#SOME_ARRAY[CE]#
$07CF#SOME_ARRAY[CF]#
$07D0#SOME_ARRAY[D0]#
$07D1#SOME_ARRAY[D1]#
$07D2#SOME_ARRAY[D2]#
$07D3#SOME_ARRAY[D3]#
$07D4#SOME_ARRAY[D4]#
$07D5#SOME_ARRAY[D5]#
$07D6#SOME_ARRAY[D6]#
$07D7#SOME_ARRAY[D7]#
$07D8#SOME_ARRAY[D8]#
$07D9#SOME_ARRAY[D9]#
$07DA#SOME_ARRAY[DA]#
$07DB#SOME_ARRAY[DB]#
$07DC#SOME_ARRAY[DC]#
$07DD#SOME_ARRAY[DD]#
$07DE#SOME_ARRAY[DE]#
$07DF#SOME_ARRAY[DF]#
$07E0#SOME_ARRAY[E0]#
$07E1#SOME_ARRAY[E1]#
$07E2#SOME_ARRAY[E2]#
$07E3#SOME_ARRAY[E3]#
$07E4#SOME_ARRAY[E4]#
$07E5#SOME_ARRAY[E5]#
$07E6#SOME_ARRAY[E6]#
$07E7#SOME_ARRAY[E7]#
$07E8#SOME_ARRAY[E8]#
$07E9#SOME_ARRAY[E9]#
$07EA#SOME_ARRAY[EA]#
$07EB#SOME_ARRAY[EB]#
$07EC#SOME_ARRAY[EC]#
$07ED#SOME_ARRAY[ED]#
$07EE#SOME_ARRAY[EE]#
$07EF#SOME_ARRAY[EF]#
$07F0#SOME_ARRAY[F0]#
$07F1#SOME_ARRAY[F1]#
$07F2#SOME_ARRAY[F2]#
$07F3#SOME_ARRAY[F3]#
$07F4#SOME_ARRAY[F4]#
$07F5#SOME_ARRAY[F5]#
$07F6#SOME_ARRAY[F6]#
$07F7#SOME_ARRAY[F7]#
$07F8#SOME_ARRAY[F8]#
$07F9#SOME_ARRAY[F9]#
$07FA#SOME_ARRAY[FA]#
$07FB#SOME_ARRAY[FB]#
$07FC#SOME_ARRAY[FC]#
$07FD#SOME_ARRAY[FD]#
$07FE#SOME_ARRAY[FE]#
$07FF#SOME_ARRAY[FF]#
$0000#TILE_UPDATES_RDPTR#In the loop near $FD23, which value is checked to see what to
\update next in the nametables. It points into a 256-byte array
\starting at $700
$02CE#pl_hand#???
$0314#top_hand#
$000F#HAND_PTR_LO#The base address for the current player's hand
$0001#TILE_UPDATES_WRPTR#The array at $700 is a circular buffer. This is where the main loop
\writes the next tile updates
$0009#FRAME_COUNTER#
$003D#GAME_STATE?#
$0010#HAND_PTR_HI#
$0039#CURRENT_PLAYER#
$0060#DRAWN_TILE#
$0026#FOUND_COMPLETE_HAND?#
$002E#WINNING_HAND_FOUND?#
$0006#SAVED_PPU_CTRL#
$0008#SOME_GLOBAL_01#Gets initialized to $08
$0017#SOME_GLOBAL_00#Gets initialized to $0
$0007#SOME_GLOBAL_02#Gets initialized to $0
$000D#SOME_GLOBAL_03#Gets initialized to 13
$000E#SOME_GLOBAL_04#Gets initialized to $3F
$001E#LAST_PPUDATA or MULTIPLAYER_ON#(I think)
$0246#WALL[0]#
$0247#WALL[1]#
$0248#WALL[2]#
$0249#WALL[3]#
$024A#WALL[4]#
$024B#WALL[5]#
$024C#WALL[6]#
$024D#WALL[7]#
$024E#WALL[8]#
$024F#WALL[9]#
$0250#WALL[A]#
$0251#WALL[B]#
$0252#WALL[C]#
$0253#WALL[D]#
$0254#WALL[E]#
$0255#WALL[F]#
$0256#WALL[10]#
$0257#WALL[11]#
$0258#WALL[12]#
$0259#WALL[13]#
$025A#WALL[14]#
$025B#WALL[15]#
$025C#WALL[16]#
$025D#WALL[17]#
$025E#WALL[18]#
$025F#WALL[19]#
$0260#WALL[1A]#
$0261#WALL[1B]#
$0262#WALL[1C]#
$0263#WALL[1D]#
$0264#WALL[1E]#
$0265#WALL[1F]#
$0266#WALL[20]#
$0267#WALL[21]#
$0268#WALL[22]#
$0269#WALL[23]#
$026A#WALL[24]#
$026B#WALL[25]#
$026C#WALL[26]#
$026D#WALL[27]#
$026E#WALL[28]#
$026F#WALL[29]#
$0270#WALL[2A]#
$0271#WALL[2B]#
$0272#WALL[2C]#
$0273#WALL[2D]#
$0274#WALL[2E]#
$0275#WALL[2F]#
$0276#WALL[30]#
$0277#WALL[31]#
$0278#WALL[32]#
$0279#WALL[33]#
$027A#WALL[34]#
$027B#WALL[35]#
$027C#WALL[36]#
$027D#WALL[37]#
$027E#WALL[38]#
$027F#WALL[39]#
$0280#WALL[3A]#
$0281#WALL[3B]#
$0282#WALL[3C]#
$0283#WALL[3D]#
$0284#WALL[3E]#
$0285#WALL[3F]#
$0286#WALL[40]#
$0287#WALL[41]#
$0288#WALL[42]#
$0289#WALL[43]#
$028A#WALL[44]#
$028B#WALL[45]#
$028C#WALL[46]#
$028D#WALL[47]#
$028E#WALL[48]#
$028F#WALL[49]#
$0290#WALL[4A]#
$0291#WALL[4B]#
$0292#WALL[4C]#
$0293#WALL[4D]#
$0294#WALL[4E]#
$0295#WALL[4F]#
$0296#WALL[50]#
$0297#WALL[51]#
$0298#WALL[52]#
$0299#WALL[53]#
$029A#WALL[54]#
$029B#WALL[55]#
$029C#WALL[56]#
$029D#WALL[57]#
$029E#WALL[58]#
$029F#WALL[59]#
$02A0#WALL[5A]#
$02A1#WALL[5B]#
$02A2#WALL[5C]#
$02A3#WALL[5D]#
$02A4#WALL[5E]#
$02A5#WALL[5F]#
$02A6#WALL[60]#
$02A7#WALL[61]#
$02A8#WALL[62]#
$02A9#WALL[63]#
$02AA#WALL[64]#
$02AB#WALL[65]#
$02AC#WALL[66]#
$02AD#WALL[67]#
$02AE#WALL[68]#
$02AF#WALL[69]#
$02B0#WALL[6A]#
$02B1#WALL[6B]#
$02B2#WALL[6C]#
$02B3#WALL[6D]#
$02B4#WALL[6E]#
$02B5#WALL[6F]#
$02B6#WALL[70]#
$02B7#WALL[71]#
$02B8#WALL[72]#
$02B9#WALL[73]#
$02BA#WALL[74]#
$02BB#WALL[75]#
$02BC#WALL[76]#
$02BD#WALL[77]#
$02BE#WALL[78]#
$02BF#WALL[79]#
$02C0#WALL[7A]#
$02C1#WALL[7B]#
$02C2#WALL[7C]#
$02C3#WALL[7D]#
$02C4#WALL[7E]#
$02C5#WALL[7F] aka Dora indicator#
$02C6#WALL[80]#
$02C7#WALL[81]#
$02C8#WALL[82]#
$02C9#WALL[83]#
$02CA#WALL[84]#
$02CB#WALL[85]#
$02CC#WALL[86]#
$02CD#WALL[87]#
$0022#CUR_TILE#
$0312#PLAYER_SCORE_HI#
$0311#PLAYER_SCORE_LO#
$0358#TOP_AI_SCORE_HI#
$0357#TOP_AI_SCORE_LO#
$039E#TOPMID_AI_SCORE_HI#
$039D#TOPMID_AI_SCORE_LO#
$03E4#BOTMID_AI_SCORE_HI#
$03E3#BOTMID_AI_SCORE_LO#
$001D#TILES_LEFT_IN_WALL#
$048F#TILES_LEFT_PER_TYPE[0]#Number of tiles left of each type
$0490#MAN_LEFT_GLOBAL[1]#
$0491#MAN_LEFT_GLOBAL[2]#
$0492#MAN_LEFT_GLOBAL[3]#
$0493#MAN_LEFT_GLOBAL[4]#Number of tiles left of each type
$0494#MAN_LEFT_GLOBAL[5]#Number of tiles left of each type
$0495#MAN_LEFT_GLOBAL[6]#Number of tiles left of each type
$0496#MAN_LEFT_GLOBAL[7]#Number of tiles left of each type
$0497#MAN_LEFT_GLOBAL[8]#Number of tiles left of each type
$0498#MAN_LEFT_GLOBAL[9]#Number of tiles left of each type
$0499#TILES_LEFT_PER_TYPE[A]#Number of tiles left of each type
$049A#TILES_LEFT_PER_TYPE[B]#Number of tiles left of each type
$049B#TILES_LEFT_PER_TYPE[C]#Number of tiles left of each type
$049C#TILES_LEFT_PER_TYPE[D]#Number of tiles left of each type
$049D#TILES_LEFT_PER_TYPE[E]#Number of tiles left of each type
$049E#TILES_LEFT_PER_TYPE[F]#Number of tiles left of each type
$049F#TILES_LEFT_PER_TYPE[10]#Number of tiles left of each type
$04A0#PIN_LEFT_GLOBAL[1]#
$04A1#PIN_LEFT_GLOBAL[2]#Number of tiles left of each type
$04A2#PIN_LEFT_GLOBAL[3]#Number of tiles left of each type
$04A3#PIN_LEFT_GLOBAL[4]#Number of tiles left of each type
$04A4#PIN_LEFT_GLOBAL[5]#Number of tiles left of each type
$04A5#PIN_LEFT_GLOBAL[6]#Number of tiles left of each type
$04A6#PIN_LEFT_GLOBAL[7]#Number of tiles left of each type
$04A7#PIN_LEFT_GLOBAL[8]#Number of tiles left of each type
$04A8#PIN_LEFT_GLOBAL[9]#Number of tiles left of each type
$04A9#TILES_LEFT_PER_TYPE[1A]#Number of tiles left of each type
$04AA#TILES_LEFT_PER_TYPE[1B]#Number of tiles left of each type
$04AB#TILES_LEFT_PER_TYPE[1C]#Number of tiles left of each type
$04AC#TILES_LEFT_PER_TYPE[1D]#Number of tiles left of each type
$04AD#TILES_LEFT_PER_TYPE[1E]#Number of tiles left of each type
$04AE#TILES_LEFT_PER_TYPE[1F]#Number of tiles left of each type
$04AF#TILES_LEFT_PER_TYPE[20]#Number of tiles left of each type
$04B0#SOU_LEFT_GLOBAL[1]#
$04B1#SOU_LEFT_GLOBAL[2]#Number of tiles left of each type
$04B2#SOU_LEFT_GLOBAL[3]#Number of tiles left of each type
$04B3#SOU_LEFT_GLOBAL[4]#Number of tiles left of each type
$04B4#SOU_LEFT_GLOBAL[5]#Number of tiles left of each type
$04B5#SOU_LEFT_GLOBAL[6]#Number of tiles left of each type
$04B6#SOU_LEFT_GLOBAL[7]#Number of tiles left of each type
$04B7#SOU_LEFT_GLOBAL[8]#Number of tiles left of each type
$04B8#SOU_LEFT_GLOBAL[9]#Number of tiles left of each type
$04B9#TILES_LEFT_PER_TYPE[2A]#Number of tiles left of each type
$04BA#TILES_LEFT_PER_TYPE[2B]#Number of tiles left of each type
$04BB#TILES_LEFT_PER_TYPE[2C]#Number of tiles left of each type
$04BC#TILES_LEFT_PER_TYPE[2D]#Number of tiles left of each type
$04BD#TILES_LEFT_PER_TYPE[2E]#Number of tiles left of each type
$04BE#TILES_LEFT_PER_TYPE[2F]#Number of tiles left of each type
$04BF#TILES_LEFT_PER_TYPE[30]#Number of tiles left of each type
$04C0#EAST_LEFT_GLOBAL#
$04C1#SOUTH_LEFT_GLOBAL#
$04C2#WEST_LEFT_GLOBAL#
$04C3#NORTH_LEFT_GLOBAL#
$04C4#GREEN_DRAGON_LEFT_GLOBAL#
$04C5#RED_DRAGON_LEFT_GLOBAL#
$0307#PLAYER_NUM_CONCEALED_TILES#
$0012#DEMO_MODE#
$0021#NEXT_DRAW_INDEX#
$0034#LIVE_WALL_TILES_LEFT#
$023A#TMP_HAND_NUM_CALLED_SETS#
$0200#TMP_HAND#This is where a lot of the scratchwork is done. The actual player
\hands are copied here when it's the player's turn so that the 
\sorted version can stay separate
$0066#FN_RET_VALUE#Not used by all functions, and might be used for other purposes
$0310#PLHAND_TENPAI_HEURISTIC#Is 8 if in tenpai. 
$0356#TOPHAND_TENPAI_HEURISTIC#
$039C#TOPMID_TENPAI_HEURISTIC#
$03E2#BOTMID_TENPAI_HEURISTIC#
$034D#TOPHAND_CONCEALED_TILES?#
$020F#TMP_HAND_CALLED_SETS[0]#
$02DD#PLHAND_CALLED_SETS[0]#
$0210#TMP_HAND_CALLED_SETS[1]#
$0211#TMP_HAND_CALLED_SETS[2]#
$0212#TMP_HAND_CALLED_SETS[3]#
$0213#TMP_HAND_CALLED_SETS[4]#
$0214#TMP_HAND_CALLED_SETS[5]#
$0215#TMP_HAND_CALLED_SETS[6]#
$0216#TMP_HAND_CALLED_SETS[7]#
$0217#TMP_HAND_CALLED_SETS[8]#
$0218#TMP_HAND_CALLED_SETS[9]#
$0219#TMP_HAND_CALLED_SETS[A]#
$021A#TMP_HAND_CALLED_SETS[B]#
$021B#TMP_HAND_CALLED_SETS[C]#
$02DE#PLHAND_CALLED_SETS[1]#
$02DF#PLHAND_CALLED_SETS[2]#
$02E0#PLHAND_CALLED_SETS[3]#
$02E1#PLHAND_CALLED_SETS[4]#
$02E2#PLHAND_CALLED_SETS[5]#
$02E3#PLHAND_CALLED_SETS[6]#
$02E4#PLHAND_CALLED_SETS[7]#
$02E5#PLHAND_CALLED_SETS[8]#
$02E6#PLHAND_CALLED_SETS[9]#
$02E7#PLHAND_CALLED_SETS[A]#
$02E8#PLHAND_CALLED_SETS[B]#
$02E9#PLHAND_CALLED_SETS[C]#
$044E#UNIQ_SORTED_HAND_CPY[0]#
$044F#UNIQ_SORTED_HAND_CPY[1]#
$0450#UNIQ_SORTED_HAND_CPY[2]#
$0451#UNIQ_SORTED_HAND_CPY[3]#
$0452#UNIQ_SORTED_HAND_CPY[4]#
$0453#UNIQ_SORTED_HAND_CPY[5]#
$0454#UNIQ_SORTED_HAND_CPY[6]#
$0455#UNIQ_SORTED_HAND_CPY[7]#
$0456#UNIQ_SORTED_HAND_CPY[8]#
$0457#UNIQ_SORTED_HAND_CPY[9]#
$0458#UNIQ_SORTED_HAND_CPY[A]#
$0459#UNIQ_SORTED_HAND_CPY[B]#
$045A#UNIQ_SORTED_HAND_CPY[C]#
$045D#TMP_HAND_RAW[0]#Once in a while we'll copy the tmp_hand here, and when we do
\so, we will unravel all its called sets and append them to the end.
\I guess the point here is to have a nice, contiguous array of what
\tiles are actual;y in the hand
$045E#TMP_HAND_RAW[1]#
$045F#TMP_HAND_RAW[2]#
$0460#TMP_HAND_RAW[3]#
$0461#TMP_HAND_RAW[4]#
$0462#TMP_HAND_RAW[5]#
$0463#TMP_HAND_RAW[6]#
$0464#TMP_HAND_RAW[7]#
$0465#TMP_HAND_RAW[8]#
$0466#TMP_HAND_RAW[9]#
$0467#TMP_HAND_RAW[A]#
$0468#TMP_HAND_RAW[B]#
$0469#TMP_HAND_RAW[C]#
$046A#TMP_HAND_RAW[D]#
$046B#TMP_HAND_RAW[E]#
$046C#TMP_HAND_RAW[F]#
$046D#TMP_HAND_RAW[10]#
$0231#TMP_HAND_FOUND_YAKU_FLAGS?#Seems like this caches the result of finding certain yaku
$04CF#CUR_PLAYER_TILES_LEFT_PER_TYPE[0]#The current player's view of how many tiles are left per type
$04D0#MAN_LEFT_PLAYER[1]#
$04D1#MAN_LEFT_PLAYER[2]#
$04D2#MAN_LEFT_PLAYER[3]#
$04D3#MAN_LEFT_PLAYER[4]#
$04D4#MAN_LEFT_PLAYER[5]#
$04D5#MAN_LEFT_PLAYER[6]#
$04D6#MAN_LEFT_PLAYER[7]#
$04D7#MAN_LEFT_PLAYER[8]#
$04D8#MAN_LEFT_PLAYER[9]#
$04D9#CUR_PLAYER_TILES_LEFT_PER_TYPE[A]#
$04DA#CUR_PLAYER_TILES_LEFT_PER_TYPE[B]#
$04DB#CUR_PLAYER_TILES_LEFT_PER_TYPE[C]#
$04DC#CUR_PLAYER_TILES_LEFT_PER_TYPE[D]#
$04DD#CUR_PLAYER_TILES_LEFT_PER_TYPE[E]#
$04DE#CUR_PLAYER_TILES_LEFT_PER_TYPE[F]#
$04DF#CUR_PLAYER_TILES_LEFT_PER_TYPE[10]#
$04E0#PIN_LEFT_PLAYER[1]#
$04E1#PIN_LEFT_PLAYER[2]#
$04E2#PIN_LEFT_PLAYER[3]#
$04E3#PIN_LEFT_PLAYER[4]#
$04E4#PIN_LEFT_PLAYER[5]#
$04E5#PIN_LEFT_PLAYER[6]#
$04E6#PIN_LEFT_PLAYER[7]#
$04E7#PIN_LEFT_PLAYER[8]#
$04E8#PIN_LEFT_PLAYER[9]#
$04E9#CUR_PLAYER_TILES_LEFT_PER_TYPE[1A]#
$04EA#CUR_PLAYER_TILES_LEFT_PER_TYPE[1B]#
$04EB#CUR_PLAYER_TILES_LEFT_PER_TYPE[1C]#
$04EC#CUR_PLAYER_TILES_LEFT_PER_TYPE[1D]#
$04ED#CUR_PLAYER_TILES_LEFT_PER_TYPE[1E]#
$04EE#CUR_PLAYER_TILES_LEFT_PER_TYPE[1F]#
$04EF#CUR_PLAYER_TILES_LEFT_PER_TYPE[20]#
$04F0#SOU_LEFT_PLAYER[1]#
$04F1#SOU_LEFT_PLAYER[2]#
$04F2#SOU_LEFT_PLAYER[3]#
$04F3#SOU_LEFT_PLAYER[4]#
$04F4#SOU_LEFT_PLAYER[5]#
$04F5#SOU_LEFT_PLAYER[6]#
$04F6#SOU_LEFT_PLAYER[7]#
$04F7#SOU_LEFT_PLAYER[8]#
$04F8#SOU_LEFT_PLAYER[9]#
$04F9#CUR_PLAYER_TILES_LEFT_PER_TYPE[2A]#
$04FA#CUR_PLAYER_TILES_LEFT_PER_TYPE[2B]#
$04FB#CUR_PLAYER_TILES_LEFT_PER_TYPE[2C]#
$04FC#CUR_PLAYER_TILES_LEFT_PER_TYPE[2D]#
$04FD#CUR_PLAYER_TILES_LEFT_PER_TYPE[2E]#
$04FE#CUR_PLAYER_TILES_LEFT_PER_TYPE[2F]#
$04FF#CUR_PLAYER_TILES_LEFT_PER_TYPE[30]#
$0500#EAST_LEFT_PLAYER#
$0501#SOUTH_LEFT_PLAYER#
$0502#WEST_LEFT_PLAYER#
$0503#NORTH_LEFT_PLAYER#
$0504#GREEN_DRAGON_LEFT_PLAYER#
$0505#RED_DRAGON_LEFT_PLAYER#
$0506#WHITE_DRAGON_LEFT_PLAYER#
$0507#CUR_PLAYER_TILES_LEFT_PER_TYPE[38]#
$0508#CUR_PLAYER_TILES_LEFT_PER_TYPE[39]#
$0509#CUR_PLAYER_TILES_LEFT_PER_TYPE[3A]#
$050A#CUR_PLAYER_TILES_LEFT_PER_TYPE[3B]#
$050B#CUR_PLAYER_TILES_LEFT_PER_TYPE[3C]#
$050C#CUR_PLAYER_TILES_LEFT_PER_TYPE[3D]#
$050D#CUR_PLAYER_TILES_LEFT_PER_TYPE[3E]#
$050E#CUR_PLAYER_TILES_LEFT_PER_TYPE[3F]#
$04C6#WHITE_DRAGON_LEFT_GLOBAL#
$050F#DISCARD_HEURISTIC_ARRAY[0]#If I had to guess, a smaller number implies a better discard
\decision
$0510#DISCARD_HEURISTIC_ARRAY?[1]#
$0511#DISCARD_HEURISTIC_ARRAY?[2]#
$0512#DISCARD_HEURISTIC_ARRAY?[3]#
$0513#DISCARD_HEURISTIC_ARRAY?[4]#
$0514#DISCARD_HEURISTIC_ARRAY?[5]#
$0515#DISCARD_HEURISTIC_ARRAY?[6]#
$0516#DISCARD_HEURISTIC_ARRAY?[7]#
$0517#DISCARD_HEURISTIC_ARRAY?[8]#
$0518#DISCARD_HEURISTIC_ARRAY?[9]#
$0519#DISCARD_HEURISTIC_ARRAY?[A]#
$051A#DISCARD_HEURISTIC_ARRAY?[B]#
$051B#DISCARD_HEURISTIC_ARRAY?[C]#
$051C#DISCARD_HEURISTIC_ARRAY?[D]#
$051D#DISCARD_HEURISTIC_ARRAY?[E]#
$051E#DISCARD_HEURISTIC_ARRAY?[F]#
$051F#DISCARD_HEURISTIC_ARRAY?[10]#
$0520#DISCARD_HEURISTIC_ARRAY?[11]#
$0521#DISCARD_HEURISTIC_ARRAY?[12]#
$0522#DISCARD_HEURISTIC_ARRAY?[13]#
$0523#DISCARD_HEURISTIC_ARRAY?[14]#
$0524#DISCARD_HEURISTIC_ARRAY?[15]#
$0525#DISCARD_HEURISTIC_ARRAY?[16]#
$0526#DISCARD_HEURISTIC_ARRAY?[17]#
$0527#DISCARD_HEURISTIC_ARRAY?[18]#
$0528#DISCARD_HEURISTIC_ARRAY?[19]#
$0529#DISCARD_HEURISTIC_ARRAY?[1A]#
$052A#DISCARD_HEURISTIC_ARRAY?[1B]#
$052B#DISCARD_HEURISTIC_ARRAY?[1C]#
$052C#DISCARD_HEURISTIC_ARRAY?[1D]#
$052D#DISCARD_HEURISTIC_ARRAY?[1E]#
$052E#DISCARD_HEURISTIC_ARRAY?[1F]#
$052F#DISCARD_HEURISTIC_ARRAY?[20]#
$0530#DISCARD_HEURISTIC_ARRAY?[21]#
$0531#DISCARD_HEURISTIC_ARRAY?[22]#
$0532#DISCARD_HEURISTIC_ARRAY?[23]#
$0533#DISCARD_HEURISTIC_ARRAY?[24]#
$0534#DISCARD_HEURISTIC_ARRAY?[25]#
$0535#DISCARD_HEURISTIC_ARRAY?[26]#
$0536#DISCARD_HEURISTIC_ARRAY?[27]#
$0537#DISCARD_HEURISTIC_ARRAY?[28]#
$0538#DISCARD_HEURISTIC_ARRAY?[29]#
$0539#DISCARD_HEURISTIC_ARRAY?[2A]#
$053A#DISCARD_HEURISTIC_ARRAY?[2B]#
$053B#DISCARD_HEURISTIC_ARRAY?[2C]#
$053C#DISCARD_HEURISTIC_ARRAY?[2D]#
$053D#DISCARD_HEURISTIC_ARRAY?[2E]#
$053E#DISCARD_HEURISTIC_ARRAY?[2F]#
$053F#DISCARD_HEURISTIC_ARRAY?[30]#
$0540#DISCARD_HEURISTIC_ARRAY?[31]#
$0541#DISCARD_HEURISTIC_ARRAY?[32]#
$0542#DISCARD_HEURISTIC_ARRAY?[33]#
$0543#DISCARD_HEURISTIC_ARRAY?[34]#
$0544#DISCARD_HEURISTIC_ARRAY?[35]#
$0545#DISCARD_HEURISTIC_ARRAY?[36]#
$0546#DISCARD_HEURISTIC_ARRAY?[37]#
$0547#DISCARD_HEURISTIC_ARRAY?[38]#
$0548#DISCARD_HEURISTIC_ARRAY?[39]#
$0549#DISCARD_HEURISTIC_ARRAY?[3A]#
$054A#DISCARD_HEURISTIC_ARRAY?[3B]#
$054B#DISCARD_HEURISTIC_ARRAY?[3C]#
$054C#DISCARD_HEURISTIC_ARRAY?[3D]#
$054D#DISCARD_HEURISTIC_ARRAY?[3E]#
$054E#DISCARD_HEURISTIC_ARRAY?[3F]#
$054F#SOME_PER_TYPE_ARRAY2[0]#
$0550#SOME_PER_TYPE_ARRAY2[1]#
$0551#SOME_PER_TYPE_ARRAY2[2]#
$0552#SOME_PER_TYPE_ARRAY2[3]#
$0553#SOME_PER_TYPE_ARRAY2[4]#
$0554#SOME_PER_TYPE_ARRAY2[5]#
$0555#SOME_PER_TYPE_ARRAY2[6]#
$0556#SOME_PER_TYPE_ARRAY2[7]#
$0557#SOME_PER_TYPE_ARRAY2[8]#
$0558#SOME_PER_TYPE_ARRAY2[9]#
$0559#SOME_PER_TYPE_ARRAY2[A]#
$055A#SOME_PER_TYPE_ARRAY2[B]#
$055B#SOME_PER_TYPE_ARRAY2[C]#
$055C#SOME_PER_TYPE_ARRAY2[D]#
$055D#SOME_PER_TYPE_ARRAY2[E]#
$055E#SOME_PER_TYPE_ARRAY2[F]#
$055F#SOME_PER_TYPE_ARRAY2[10]#
$0560#SOME_PER_TYPE_ARRAY2[11]#
$0561#SOME_PER_TYPE_ARRAY2[12]#
$0562#SOME_PER_TYPE_ARRAY2[13]#
$0563#SOME_PER_TYPE_ARRAY2[14]#
$0564#SOME_PER_TYPE_ARRAY2[15]#
$0565#SOME_PER_TYPE_ARRAY2[16]#
$0566#SOME_PER_TYPE_ARRAY2[17]#
$0567#SOME_PER_TYPE_ARRAY2[18]#
$0568#SOME_PER_TYPE_ARRAY2[19]#
$0569#SOME_PER_TYPE_ARRAY2[1A]#
$056A#SOME_PER_TYPE_ARRAY2[1B]#
$056B#SOME_PER_TYPE_ARRAY2[1C]#
$056C#SOME_PER_TYPE_ARRAY2[1D]#
$056D#SOME_PER_TYPE_ARRAY2[1E]#
$056E#SOME_PER_TYPE_ARRAY2[1F]#
$056F#SOME_PER_TYPE_ARRAY2[20]#
$0570#SOME_PER_TYPE_ARRAY2[21]#
$0571#SOME_PER_TYPE_ARRAY2[22]#
$0572#SOME_PER_TYPE_ARRAY2[23]#
$0573#SOME_PER_TYPE_ARRAY2[24]#
$0574#SOME_PER_TYPE_ARRAY2[25]#
$0575#SOME_PER_TYPE_ARRAY2[26]#
$0576#SOME_PER_TYPE_ARRAY2[27]#
$0577#SOME_PER_TYPE_ARRAY2[28]#
$0578#SOME_PER_TYPE_ARRAY2[29]#
$0579#SOME_PER_TYPE_ARRAY2[2A]#
$057A#SOME_PER_TYPE_ARRAY2[2B]#
$057B#SOME_PER_TYPE_ARRAY2[2C]#
$057C#SOME_PER_TYPE_ARRAY2[2D]#
$057D#SOME_PER_TYPE_ARRAY2[2E]#
$057E#SOME_PER_TYPE_ARRAY2[2F]#
$057F#SOME_PER_TYPE_ARRAY2[30]#
$0580#SOME_PER_TYPE_ARRAY2[31]#
$0581#SOME_PER_TYPE_ARRAY2[32]#
$0582#SOME_PER_TYPE_ARRAY2[33]#
$0583#SOME_PER_TYPE_ARRAY2[34]#
$0584#SOME_PER_TYPE_ARRAY2[35]#
$0585#SOME_PER_TYPE_ARRAY2[36]#
$0586#SOME_PER_TYPE_ARRAY2[37]#
$0587#SOME_PER_TYPE_ARRAY2[38]#
$0588#SOME_PER_TYPE_ARRAY2[39]#
$0589#SOME_PER_TYPE_ARRAY2[3A]#
$058A#SOME_PER_TYPE_ARRAY2[3B]#
$058B#SOME_PER_TYPE_ARRAY2[3C]#
$058C#SOME_PER_TYPE_ARRAY2[3D]#
$058D#SOME_PER_TYPE_ARRAY2[3E]#
$058E#SOME_PER_TYPE_ARRAY2[3F]#
$004B#DISCARD_CANDIDATE#
$058F#TENPAI_SCORES_PER_TILE_TYPE[0]#
$0590#TENPAI_SCORES_PER_TILE_TYPE[1]#
$0591#TENPAI_SCORES_PER_TILE_TYPE[2]#
$0592#TENPAI_SCORES_PER_TILE_TYPE[3]#
$0593#TENPAI_SCORES_PER_TILE_TYPE[4]#
$0594#TENPAI_SCORES_PER_TILE_TYPE[5]#
$0595#TENPAI_SCORES_PER_TILE_TYPE[6]#
$0596#TENPAI_SCORES_PER_TILE_TYPE[7]#
$0597#TENPAI_SCORES_PER_TILE_TYPE[8]#
$0598#TENPAI_SCORES_PER_TILE_TYPE[9]#
$0599#TENPAI_SCORES_PER_TILE_TYPE[A]#
$059A#TENPAI_SCORES_PER_TILE_TYPE[B]#
$059B#TENPAI_SCORES_PER_TILE_TYPE[C]#
$059C#TENPAI_SCORES_PER_TILE_TYPE[D]#
$059D#TENPAI_SCORES_PER_TILE_TYPE[E]#
$059E#TENPAI_SCORES_PER_TILE_TYPE[F]#
$059F#TENPAI_SCORES_PER_TILE_TYPE[10]#
$05A0#TENPAI_SCORES_PER_TILE_TYPE[11]#
$05A1#TENPAI_SCORES_PER_TILE_TYPE[12]#
$05A2#TENPAI_SCORES_PER_TILE_TYPE[13]#
$05A3#TENPAI_SCORES_PER_TILE_TYPE[14]#
$05A4#TENPAI_SCORES_PER_TILE_TYPE[15]#
$05A5#TENPAI_SCORES_PER_TILE_TYPE[16]#
$05A6#TENPAI_SCORES_PER_TILE_TYPE[17]#
$05A7#TENPAI_SCORES_PER_TILE_TYPE[18]#
$05A8#TENPAI_SCORES_PER_TILE_TYPE[19]#
$05A9#TENPAI_SCORES_PER_TILE_TYPE[1A]#
$05AA#TENPAI_SCORES_PER_TILE_TYPE[1B]#
$05AB#TENPAI_SCORES_PER_TILE_TYPE[1C]#
$05AC#TENPAI_SCORES_PER_TILE_TYPE[1D]#
$05AD#TENPAI_SCORES_PER_TILE_TYPE[1E]#
$05AE#TENPAI_SCORES_PER_TILE_TYPE[1F]#
$05AF#TENPAI_SCORES_PER_TILE_TYPE[20]#
$05B0#TENPAI_SCORES_PER_TILE_TYPE[21]#
$05B1#TENPAI_SCORES_PER_TILE_TYPE[22]#
$05B2#TENPAI_SCORES_PER_TILE_TYPE[23]#
$05B3#TENPAI_SCORES_PER_TILE_TYPE[24]#
$05B4#TENPAI_SCORES_PER_TILE_TYPE[25]#
$05B5#TENPAI_SCORES_PER_TILE_TYPE[26]#
$05B6#TENPAI_SCORES_PER_TILE_TYPE[27]#
$05B7#TENPAI_SCORES_PER_TILE_TYPE[28]#
$05B8#TENPAI_SCORES_PER_TILE_TYPE[29]#
$05B9#TENPAI_SCORES_PER_TILE_TYPE[2A]#
$05BA#TENPAI_SCORES_PER_TILE_TYPE[2B]#
$05BB#TENPAI_SCORES_PER_TILE_TYPE[2C]#
$05BC#TENPAI_SCORES_PER_TILE_TYPE[2D]#
$05BD#TENPAI_SCORES_PER_TILE_TYPE[2E]#
$05BE#TENPAI_SCORES_PER_TILE_TYPE[2F]#
$05BF#TENPAI_SCORES_PER_TILE_TYPE[30]#
$05C0#TENPAI_SCORES_PER_TILE_TYPE[31]#
$05C1#TENPAI_SCORES_PER_TILE_TYPE[32]#
$05C2#TENPAI_SCORES_PER_TILE_TYPE[33]#
$05C3#TENPAI_SCORES_PER_TILE_TYPE[34]#
$05C4#TENPAI_SCORES_PER_TILE_TYPE[35]#
$05C5#TENPAI_SCORES_PER_TILE_TYPE[36]#
$05CF#BEST_DISCARDS_LEN#
$05D0#BEST_DISCARDS_LIST[0]#
$05D1#BEST_DISCARDS_LIST[1]#
$05D2#BEST_DISCARDS_LIST[2]#
$05D3#BEST_DISCARDS_LIST[3]#
$05D4#BEST_DISCARDS_LIST[4]#
$05D5#BEST_DISCARDS_LIST[5]#
$05D6#BEST_DISCARDS_LIST[6]#
$05D7#BEST_DISCARDS_LIST[7]#
$05D8#BEST_DISCARDS_LIST[8]#
$05D9#BEST_DISCARDS_LIST[9]#
$05DA#BEST_DISCARDS_LIST[A]#
$05DB#BEST_DISCARDS_LIST[B]#
$05DC#BEST_DISCARDS_LIST[C]#
